package com.kk.demo.core.utils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @author AaronXu
 * @date 2021/11/15 15:25
 */
public class HmacSha256Utils {

    public static String getHmacSha256Str(String data, String key) throws Exception {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
        sha256_HMAC.init(secret_key);

        byte[] array = sha256_HMAC.doFinal(data.getBytes("UTF-8"));

        StringBuilder sb = new StringBuilder();

        for (byte item : array) {

            sb.append(Integer.toHexString((item & 0xFF) | 0x100).substring(1, 3));
        }
        return sb.toString().toUpperCase();
    }

    public static String hmacSha256(String value,String key){
        String result = null;
        byte[] keyBytes = key.getBytes();
        SecretKeySpec localMac = new SecretKeySpec(keyBytes, "HmacSHA256");
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(localMac);
            byte[] arrayOfByte = mac.doFinal(value.getBytes());
            BigInteger localBigInteger = new BigInteger(1,
                    arrayOfByte);
            result = String.format("%0" + (arrayOfByte.length << 1) + "x",
                    new Object[] { localBigInteger });

        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        return result;
    }
}
