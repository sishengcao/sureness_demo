package com.kk.demo.core.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @date: 2020/7/24 12:49
 * @Author: AaronXu
 */
@Data
public class BasePage<S> implements Serializable {
	private static final long serialVersionUID = 1L;

	// 前端传入-每页显示多少
	private int pageSize = 10;
	// 前端传入-页码
	private int pageNum = 1;
}
