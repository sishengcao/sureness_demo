package com.kk.demo.core.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatVisitorWrapper;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/07/14 20:07
 */
public class BigDecimalToStringSerializer extends StdSerializer<BigDecimal> {
    public static final BigDecimalToStringSerializer instance = new BigDecimalToStringSerializer();

    public BigDecimalToStringSerializer() {
        super(BigDecimal.class);
    }

    @Override
    public boolean isEmpty(SerializerProvider prov, BigDecimal value) {
        return value == null;
    }

    @Override
    public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeString(value.toPlainString());
    }

    @Override
    public void serializeWithType(BigDecimal value, JsonGenerator g, SerializerProvider provider,
                                  TypeSerializer typeSer)
            throws IOException {
        serialize(value, g, provider);
    }

    public JsonNode getSchema(SerializerProvider provider, Type typeHint) throws JsonMappingException {
        return createSchemaNode("string", true);
    }

    public void acceptJsonFormatVisitor(JsonFormatVisitorWrapper visitor, JavaType typeHint) throws JsonMappingException {
        //json类型是String
        visitStringFormat(visitor, typeHint);
    }
}