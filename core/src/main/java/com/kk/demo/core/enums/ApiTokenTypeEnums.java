package com.kk.demo.core.enums;

import lombok.Getter;

/**
 * @author: ziHeng
 * @description:
 * @create: 2020/12/25 16:14
 */
@Getter
public enum ApiTokenTypeEnums {

    APP(1, "APP", 60 * 60 * 24 * 15),
    SYSTEM(2, "SYSTEM", 60 * 60 * 24 * 15),
    WEB(3, "WEB", 60 * 60 * 24 * 15),
    THIRD_PARTY(4, "THIRD_PARTY", 60 * 5);

    private Integer type;

    private String value;

    //token过期时间 单位:秒
    private long tokenExpirationTime;


    ApiTokenTypeEnums(Integer type, String value, long tokenExpirationTime) {
        this.type = type;
        this.value = value;
        this.tokenExpirationTime = tokenExpirationTime;
    }
}
