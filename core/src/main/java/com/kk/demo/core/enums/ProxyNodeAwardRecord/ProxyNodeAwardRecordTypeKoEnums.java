package com.kk.demo.core.enums.ProxyNodeAwardRecord;

import lombok.Getter;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/08/29 14:22
 */
@Getter
public enum ProxyNodeAwardRecordTypeKoEnums {

    COMMISSION(1, "일반 노드 - 인센티브"),
    CENT(2, "일반 노드 - 센트코인"),
    SUPER_NODE_COMISSION(3, "슈퍼노드 - 인센티브"),
    SUPER_NODE_CENT(4, "슈퍼노드 - 센트코인"),


    COMMISSION_PEERS(10, "인발노드 - 인센티브(동급)"),
    CENT_PEERS(20, "일반노드 - 센트코인(동급)"),
    SUPER_NODE_COMISSION_PEERS(30, "슈퍼노드 - 인센티브(동급)"),
    SUPER_NODE_CENT_PEERS(40, "슈퍼노드 - 센트코인(동급)");

    private Integer type;

    private String name;

    ProxyNodeAwardRecordTypeKoEnums(Integer type, String name) {
        this.type = type;
        this.name = name;
    }
}
