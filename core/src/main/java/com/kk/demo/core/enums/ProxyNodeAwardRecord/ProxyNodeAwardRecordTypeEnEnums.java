package com.kk.demo.core.enums.ProxyNodeAwardRecord;

import lombok.Getter;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/08/29 14:22
 */
@Getter
public enum ProxyNodeAwardRecordTypeEnEnums {

    COMMISSION(1, "Common node - commission"),
    CENT(2, "Common node - coin distribution"),
    SUPER_NODE_COMISSION(3, "Super node - commission"),
    SUPER_NODE_CENT(4, "Super node - coin distribution"),


    COMMISSION_PEERS(10, "Common node - commission (the same level)"),
    CENT_PEERS(20, "Common node - coin distribution (the same level)"),
    SUPER_NODE_COMISSION_PEERS(30, "Super node - commission (the same level)"),
    SUPER_NODE_CENT_PEERS(40, "Super node - coin distribution (the same level)");

    private Integer type;

    private String name;

    ProxyNodeAwardRecordTypeEnEnums(Integer type, String name) {
        this.type = type;
        this.name = name;
    }
}
