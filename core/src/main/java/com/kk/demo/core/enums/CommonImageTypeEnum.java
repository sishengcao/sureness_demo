package com.kk.demo.core.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import com.kk.demo.core.exceptions.BusinessException;
import lombok.Getter;

/**
 * @ClassName CommonImageTypeEnum
 * @Description 图片类型
 * @Author July
 * @Date: 2021-08-09 14:58:55
 **/
@Getter
public enum CommonImageTypeEnum {
    ALL(0, "全部"),
    ABOUT_US(1, "关于我们"),
    MINING_MACHINE_MANAGED(2, "矿机托管"),
    BANNER(3, "轮播图"),
    VIDEO(4, "视频"),
    H5_BANNER(5, "H5轮播图"),
    ;

    @EnumValue
    private final Integer value;
    private final String describe;

    CommonImageTypeEnum(Integer value, String describe) {
        this.value = value;
        this.describe = describe;
    }

    public String getName() {
        return this.name().toUpperCase();
    }

    @JsonValue
    public Integer getValue() {
        return value;
    }

    public String getDescribe() {
        return describe;
    }

    public static CommonImageTypeEnum fromName(String name) {
        if (null == name) {
            return null;
        }
        for (CommonImageTypeEnum b : CommonImageTypeEnum.values()) {
            if (b.getName().equalsIgnoreCase(name)) {
                return b;
            }
        }
        throw new BusinessException("不存在这种图片类型:" + name);
    }

    public static CommonImageTypeEnum fromDescribe(String describe) {
        if (null == describe) {
            return null;
        }
        for (CommonImageTypeEnum b : CommonImageTypeEnum.values()) {
            if (b.getDescribe().equals(describe)) {
                return b;
            }
        }
        throw new BusinessException("不存在这种图片类型:" + describe);
    }

    public static CommonImageTypeEnum valueOf(Integer value) {
        for (CommonImageTypeEnum b : CommonImageTypeEnum.values()) {
            if (b.getValue().equals(value)) {
                return b;
            }
        }
        throw new BusinessException("不存在这种图片类型:" + value);
    }
}
