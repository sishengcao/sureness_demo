package com.kk.demo.core.utils;

import java.util.Map;

/**
 * @author: ziHeng
 * @description:
 * @create: 2020/10/27 14:11
 */
public class CheckUtil {

    public static boolean checkIdcard(String realName, String idCard) {
        String params = "key=09deef06c8afe284ef8df9a62b461a9b" + "&realname=" + realName + "&idcard=" + idCard;
        String rt = HttpRequest.sendPost("http://op.juhe.cn/idcard/query", params);
        System.out.println(rt);
        Map<String, Object> map = JsonUtils.jsonToMap(rt);
        if (map.get("result") == null) {
            return false;
        }
        Map map2 = (Map) map.get("result");
        if ("2".equals(map2.get("res").toString())) {
            return false;
        }
        return true;
    }

    public static boolean checkBank(String bankCardholder,String bankNum){
        String params = "key=c03d8a27041ca926ed91296e3e5ce6db" +"&realname="+bankCardholder+"&bankcard="+bankNum;
        String rt = HttpRequest.sendPost("http://v.juhe.cn/verifybankcard/query", params);
        System.out.println(rt);
        Map<String, Object> map = JsonUtils.jsonToMap(rt);
        if(map.get("result")==null){
            return false;
        }
        Map map2 = (Map) map.get("result");
        System.out.println(map2.get("res").toString());
        if("2".equals(map2.get("res").toString())){
            return false;
        }
        return true;
    }



    /**
     * 省份证的正则表达式^(\d{15}|\d{17}[\dx])$
     *
     * @param id 省份证号
     * @return 生日（yyyy-MM-dd）
     */
    public static String extractYearMonthDayOfIdCard(String id) {
        String year = null;
        String month = null;
        String day = null;
        //正则匹配身份证号是否是正确的，15位或者17位数字+数字/x/X
        if (id.matches("^\\d{15}|\\d{17}[\\dxX]$")) {
            year = id.substring(6, 10);
            month = id.substring(10, 12);
            day = id.substring(12, 14);
        } else {
            System.out.println("身份证号码不匹配！");
            return null;
        }
        return year + "-" + month + "-" + day;
    }

    public static void main(String[] args) {
    }

}
