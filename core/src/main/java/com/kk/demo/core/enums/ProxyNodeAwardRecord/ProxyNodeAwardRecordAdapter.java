package com.kk.demo.core.enums.ProxyNodeAwardRecord;

import com.kk.demo.core.enums.LanguageEnums;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/11/01 20:26
 */
public class ProxyNodeAwardRecordAdapter {

    private static Map<String, String> proxyNodeAwardRecordMap;

    private static Map<String, String> getProxyNodeAwardRecordMap() {
        if (proxyNodeAwardRecordMap == null) {
            synchronized (ProxyNodeAwardRecordAdapter.class) {
                if (proxyNodeAwardRecordMap == null) {
                    proxyNodeAwardRecordMap = new HashMap<>();
                    ProxyNodeAwardRecordTypeEnums[] proxyNodeAwardRecordTypeEnums = ProxyNodeAwardRecordTypeEnums.values();
                    for (ProxyNodeAwardRecordTypeEnums value : proxyNodeAwardRecordTypeEnums) {
                        proxyNodeAwardRecordMap.put(getMapKey(LanguageEnums.CHINE_OLD.getValue(),value.getType()),value.getName());
                    }
                    ProxyNodeAwardRecordTypeEnEnums[] proxyNodeAwardRecordTypeEnEnums = ProxyNodeAwardRecordTypeEnEnums.values();
                    for (ProxyNodeAwardRecordTypeEnEnums proxyNodeAwardRecordTypeEnEnum : proxyNodeAwardRecordTypeEnEnums) {
                        proxyNodeAwardRecordMap.put(getMapKey(LanguageEnums.ENGLISH.getValue(),proxyNodeAwardRecordTypeEnEnum.getType()),proxyNodeAwardRecordTypeEnEnum.getName());
                    }
                    ProxyNodeAwardRecordTypeArEnums[] proxyNodeAwardRecordTypeArEnums = ProxyNodeAwardRecordTypeArEnums.values();
                    for (ProxyNodeAwardRecordTypeArEnums proxyNodeAwardRecordTypeArEnum : proxyNodeAwardRecordTypeArEnums) {
                        proxyNodeAwardRecordMap.put(getMapKey(LanguageEnums.AR.getValue(),proxyNodeAwardRecordTypeArEnum.getType()),proxyNodeAwardRecordTypeArEnum.getName());
                    }
                    ProxyNodeAwardRecordTypeJaEnums[] proxyNodeAwardRecordTypeJaEnums = ProxyNodeAwardRecordTypeJaEnums.values();
                    for (ProxyNodeAwardRecordTypeJaEnums proxyNodeAwardRecordTypeJaEnum : proxyNodeAwardRecordTypeJaEnums) {
                        proxyNodeAwardRecordMap.put(getMapKey(LanguageEnums.JAPANESE.getValue(),proxyNodeAwardRecordTypeJaEnum.getType()),proxyNodeAwardRecordTypeJaEnum.getName());
                    }
                    ProxyNodeAwardRecordTypeKoEnums[] proxyNodeAwardRecordTypeKoEnums = ProxyNodeAwardRecordTypeKoEnums.values();
                    for (ProxyNodeAwardRecordTypeKoEnums proxyNodeAwardRecordTypeKoEnum : proxyNodeAwardRecordTypeKoEnums) {
                        proxyNodeAwardRecordMap.put(getMapKey(LanguageEnums.KO.getValue(),proxyNodeAwardRecordTypeKoEnum.getType()),proxyNodeAwardRecordTypeKoEnum.getName());
                    }
                    ProxyNodeAwardRecordTypeRuEnums[] proxyNodeAwardRecordTypeRuEnums = ProxyNodeAwardRecordTypeRuEnums.values();
                    for (ProxyNodeAwardRecordTypeRuEnums proxyNodeAwardRecordTypeRuEnum : proxyNodeAwardRecordTypeRuEnums) {
                        proxyNodeAwardRecordMap.put(getMapKey(LanguageEnums.RU.getValue(),proxyNodeAwardRecordTypeRuEnum.getType()),proxyNodeAwardRecordTypeRuEnum.getName());
                    }
                    ProxyNodeAwardRecordTypeThEnums[] proxyNodeAwardRecordTypeThEnums = ProxyNodeAwardRecordTypeThEnums.values();
                    for (ProxyNodeAwardRecordTypeThEnums proxyNodeAwardRecordTypeThEnum : proxyNodeAwardRecordTypeThEnums) {
                        proxyNodeAwardRecordMap.put(getMapKey(LanguageEnums.THAI.getValue(),proxyNodeAwardRecordTypeThEnum.getType()),proxyNodeAwardRecordTypeThEnum.getName());
                    }
                    ProxyNodeAwardRecordTypeIdEnums[] proxyNodeAwardRecordTypeIdEnums = ProxyNodeAwardRecordTypeIdEnums.values();
                    for (ProxyNodeAwardRecordTypeIdEnums proxyNodeAwardRecordTypeIdEnum : proxyNodeAwardRecordTypeIdEnums) {
                        proxyNodeAwardRecordMap.put(getMapKey(LanguageEnums.ID.getValue(),proxyNodeAwardRecordTypeIdEnum.getType()),proxyNodeAwardRecordTypeIdEnum.getName());
                    }

                }
            }
        }
        return proxyNodeAwardRecordMap;
    }

    public static String getProxyNodeAwardRemarkByTypeAndLanguage(String language,
                                                                   Integer type) {

        Map<String, String> proxyNodeAwardRecordMap = getProxyNodeAwardRecordMap();
        return proxyNodeAwardRecordMap.get(getMapKey(language,type));

    }


    private static String getMapKey(String language,
                                    Integer type) {
        return language + "-" + type;
    }

}
