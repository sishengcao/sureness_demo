package com.kk.demo.core.utils;

/**
 * @author sishengcao
 * @date 2021/5/15 12:12
 **/

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 由于Java的简单类型不能够精确的对浮点数进行运算，这个工具类提供精
 * 确的浮点数运算，包括加减乘除和四舍五入。
 */
public class ArithUtil {
    //默认除法运算精度
    private static final int DEF_DIV_SCALE = 10;
    //这个类不能实例化
    private ArithUtil(){
    }

    /**
     * 提供精确的加法运算。
     * @param v1 被加数
     * @param v2 加数
     * @return 两个参数的和
     */
    public static BigDecimal add(BigDecimal v1,BigDecimal v2){
        return v1.add(v2);
    }

    /**
     * 提供精确的减法运算。
     * @param v1 被减数
     * @param v2 减数
     * @return 两个参数的差
     */
    public static BigDecimal sub(BigDecimal v1,BigDecimal v2){
        return v1.subtract(v2);
    }

    /**
     * 提供精确的乘法运算, 在 scale 位数 向下取整
     * @param b1 被乘数
     * @param b2 乘数
     * @return 两个参数的积
     */
    public static BigDecimal mul(BigDecimal b1,BigDecimal b2,int scale){
        String result = b1.multiply(b2).setScale(scale,RoundingMode.DOWN).toPlainString();
        return new BigDecimal(result);
    }

    /**
     * 提供（相对）精确的除法运算，在 scale 位数 向下取整
     * 小数点以后10位，以后的数字四舍五入。
     * @param v1 被除数
     * @param v2 除数
     * @return 两个参数的商
     */
    public static BigDecimal div(BigDecimal v1,BigDecimal v2,int scale){
        String result = v1.divide(v2,scale, BigDecimal.ROUND_HALF_DOWN).toPlainString();
        return new BigDecimal(result);
    }

    /**
     *
     * @param v1
     * @param v2
     * @param scale
     * @param mode 取整方式
     * @return
     */
    public static BigDecimal div(BigDecimal v1,BigDecimal v2,int scale,RoundingMode mode){
        String result = v1.divide(v2).setScale(scale, mode).toPlainString();
        return new BigDecimal(result);
    }

    /**
     * 提供精确的小数位四舍五入处理。
     * @param v 需要四舍五入的数字
     * @param scale 小数点后保留几位
     * @return 四舍五入后的结果
     */
    public static double round(double v,int scale){
        if(scale<0){
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b = new BigDecimal(Double.toString(v));
        BigDecimal one = new BigDecimal("1");
        return b.divide(one,scale,BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * 四舍五入 向下取整
     * @param v
     * @return
     */
    public static int round(double v){
        BigDecimal b = new BigDecimal(Double.toString(v));
        return b.setScale(0,BigDecimal.ROUND_DOWN).intValue();
    }
}
