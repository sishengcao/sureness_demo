package com.kk.demo.core.enums.ProxyNodeAwardRecord;

import lombok.Getter;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/08/29 14:22
 */
@Getter
public enum ProxyNodeAwardRecordTypeJaEnums {

    COMMISSION(1, "ノーマルノード-コミッション"),
    CENT(2, "ノーマルノード-コイン"),
    SUPER_NODE_COMISSION(3, "スーパーノード-コミッション"),
    SUPER_NODE_CENT(4, "スーパーノード-コイン"),


    COMMISSION_PEERS(10, "ノーマルノード-コミッション（段階別）"),
    CENT_PEERS(20, "ノーマルノード-コイン（段階別）"),
    SUPER_NODE_COMISSION_PEERS(30, "スーパーノード-コミッション（段階別）"),
    SUPER_NODE_CENT_PEERS(40, "スーパーノード-コイン（段階別）");

    private Integer type;

    private String name;

    ProxyNodeAwardRecordTypeJaEnums(Integer type, String name) {
        this.type = type;
        this.name = name;
    }
}
