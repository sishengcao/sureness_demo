package com.kk.demo.core.utils.parameterCheck;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @date: 2021/6/9 11:03
 * @Author: AaronXu
 * @Description: 常用正则表达式验证，提供验证正则表达式的方法
 */
public class PatternUtil {
    /**
     * 字符创正则匹配
     */
    public static boolean match(String toCheck, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(toCheck);
        return matcher.matches();
    }

    /**
     * 正整数
     */
    public static boolean isPositiveNum(String toCheck) {
        String regex = "^([1-9][0-9]*)$";
        return match(toCheck, regex);
    }

    /**
     * 负整数，0，正整数
     */
    public static boolean isNumeric(String toCheck) {
        String regex = "^([0])|(\\-?[1-9][0-9]*)$";
        return match(toCheck, regex);
    }

    /**
     * 包含负数/小数/整数,最多8位小数,可以为正整数
     */
    public static boolean isDecimal(String toCheck) {
        String regex = "^(\\-?[0-9]*\\.?\\d{1,8})$";
        return match(toCheck, regex);
    }

    /**
     * 由26个引文字母组成的字符串
     */
    public static boolean isCharacter(String toCheck) {
        String regex = "^[A-Za-z]+$";
        return match(toCheck, regex);
    }

    /**
     * 密码验证：长度在6-14之间，以数字、字母和下划线组成
     */
    public static boolean isPassword(String toCheck) {
        String regex = "^[\\w\\_]{6,14}$";
        return match(toCheck, regex);
    }

/*    public static void main(String[] args) {
        System.out.println("金额校验:" + PatternUtil.isDecimal("12345678910110.12345678"));


        //System.out.println("密码校验:"+PatternUtil.isPassword("12345"));
    }*/

    /**
     * 电话号码验证：<br>
     * 010-12345678<br>
     * 0912-1234567<br>
     * (010)-12345678<br>
     * (0912)1234567<br>
     * (010)12345678<br>
     * (0912)-1234567<br>
     * 01012345678<br>
     * 09121234567<br>
     *
     * @param toCheck
     * @return
     */
    public static boolean isPhone(String toCheck) {
        String regex = "^(^0\\d{2}-?\\d{8}$)|(^0\\d{3}-?\\d{7}$)|(^\\(0\\d{2}\\)-?\\d{8}$)|(^\\(0\\d{3}\\)-?\\d{7}$)$";
        return match(toCheck, regex);
    }

    public static boolean checkGuojiPhone(String phone, String area) {
        if (phone.length() >= 7) {
            return true;
        }
        return false;
    }

    /**
     * 手机验证：1开头的11位数字
     */
    public static boolean isMobile(String toCheck) {
        String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(16[0-9])|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9])|(92[0-9])|(98[0-9]))\\d{8}$";
        return match(toCheck, regex);
    }

    /**
     * 电子邮箱验证
     */
    public static boolean isEmail(String toCheck) {
        String regex = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        return match(toCheck, regex);
    }



    /**
     * 身份证验证
     */
    public static boolean isIdCard(String toCheck) {
        return isIdNoPattern(toCheck) && isValidProvinceId(toCheck.substring(0, 2))
                && isValidDate(toCheck.substring(6, 14)) && checkIdNoLastNum(toCheck);
    }

    /**
     * 二代身份证正则表达式
     */
    private static boolean isIdNoPattern(String idNo) {
        return Pattern.matches("^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([\\d|x|X]{1})$", idNo);
    }

    // 省(直辖市)码表
    private static String provinceCode[] = {"11", "12", "13", "14", "15", "21", "22",
            "23", "31", "32", "33", "34", "35", "36", "37", "41", "42", "43",
            "44", "45", "46", "50", "51", "52", "53", "54", "61", "62", "63",
            "64", "65", "71", "81", "82", "91"};

    /**
     * 检查身份证的省份信息是否正确
     */
    public static boolean isValidProvinceId(String provinceId) {
        for (String id : provinceCode) {
            if (id.equals(provinceId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断日期是否有效
     */
    public static boolean isValidDate(String inDate) {
        if (inDate == null) {
            return false;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        if (inDate.trim().length() != dateFormat.toPattern().length()) {
            return false;
        }
        dateFormat.setLenient(false);//执行严格的日期匹配
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    // 身份证前17位每位加权因子
    private static int[] power = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};

    // 身份证第18位校检码
    private static String[] refNumber = {"1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"};

    /**
     * 计算身份证的第十八位校验码
     */
    public static String sumPower(int[] cardIdArray) {
        int result = 0;
        for (int i = 0; i < power.length; i++) {
            result += power[i] * cardIdArray[i];
        }
        return refNumber[(result % 11)];
    }

    /**
     * 校验身份证第18位是否正确(只适合18位身份证)
     */
    public static boolean checkIdNoLastNum(String idNo) {
        if (idNo.length() != 18) {
            return false;
        }
        char[] tmp = idNo.toCharArray();
        int[] cardidArray = new int[tmp.length - 1];
        int i = 0;
        for (i = 0; i < tmp.length - 1; i++) {
            cardidArray[i] = Integer.parseInt(tmp[i] + "");
        }
        String checkCode = sumPower(cardidArray);
        String lastNum = tmp[tmp.length - 1] + "";
        if (lastNum.equals("x")) {
            lastNum = lastNum.toUpperCase();
        }
        if (!checkCode.equals(lastNum)) {
            return false;
        }
        return true;
    }

    /**
     * 邮政编码
     */
    public static boolean isPostCode(String toCheck) {
        String regex = "^\\d{6}$";
        return match(toCheck, regex);
    }

    /**
     * IP地址验证
     */
    public static boolean isIP(String toCheck) {
        String regex = "^((([1-9]\\d?)|(1\\d{2})|(2[0-4]\\d)|(25[0-5]))\\.){3}(([1-9]\\d?)|(1\\d{2})|(2[0-4]\\d)|(25[0-5]))$";
        return match(toCheck, regex);
    }

    /**
     * 中文验证
     */
    public static boolean isChinese(String toCheck) {
        String regex = "^[\u4e00-\u9fa5]+$";
        return match(toCheck, regex);
    }

    /**
     * URL：http https和ftp
     */
    public static boolean isURL(String toCheck) {
        String regex = "^(([hH][tT]{2}[pP][sS]?)|([fF][tT][pP]))\\:\\/\\/[wW]{3}\\.[\\w-]+\\.\\w{2,4}(\\/.*)?$";
        return match(toCheck, regex);
    }

    /**
     * 日期格式：2012-05-14 2012/05/6 2012.5.14 20120528
     */
    public static boolean isDate(String toCheck) {
        String regex = "^[1-9]\\d{3}([-|\\/|\\.])?((0\\d)|([1-9])|(1[0-2]))\1(([0|1|2]\\d)|([1-9])|3[0-1])$";
        return match(toCheck, regex);
    }

    /**
     * 证件号码
     */
    public static boolean isCertify(String toCheck) {
        String regex = "^\\w*[-_.(]*\\w*[-_.)]*$";
        return match(toCheck, regex);
    }

    /**
     * 车牌号
     */
    public static boolean isCarNo(String toCheck) {
        String regex = "^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9挂学警港澳]{1}$";
        return match(toCheck, regex);
    }

    /**
     * 校验银行卡
     * 1、从卡号最后一位数字开始，逆向将奇数位(1、3、5等等)相加。
     * 2、从卡号最后一位数字开始，逆向将偶数位数字，先乘以2（如果乘积为两位数，将个位十位数字相加，即将其减去9），再求和。
     * 3、将奇数位总和加上偶数位总和，结果应该可以被10整除。
     */
    public static boolean checkBankCard(String bankCard) {
        if (bankCard.length() < 15 || bankCard.length() > 19) {
            return false;
        }
        char bit = getBankCardCheckCode(bankCard.substring(0, bankCard.length() - 1));
        if (bit == 'N') {
            return false;
        }
        return bankCard.charAt(bankCard.length() - 1) == bit;
    }

    //银行卡校验
    public static char getBankCardCheckCode(String nonCheckCodeBankCard) {
        if (nonCheckCodeBankCard == null || nonCheckCodeBankCard.trim().length() == 0
                || !nonCheckCodeBankCard.matches("\\d+")) {
            //如果传的不是数据返回N
            return 'N';
        }
        char[] chs = nonCheckCodeBankCard.trim().toCharArray();
        int luhmSum = 0;
        for (int i = chs.length - 1, j = 0; i >= 0; i--, j++) {
            int k = chs[i] - '0';
            if (j % 2 == 0) {
                k *= 2;
                k = k / 10 + k % 10;
            }
            luhmSum += k;
        }
        return (luhmSum % 10 == 0) ? '0' : (char) ((10 - luhmSum % 10) + '0');
    }
}
