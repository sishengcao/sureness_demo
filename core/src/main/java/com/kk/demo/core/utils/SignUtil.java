package com.kk.demo.core.utils;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

public class SignUtil {

    public static String getSignature(final Map<String, Object> data, String secretKey) {
        try {
            Set<String> keySet = data.keySet();
            String[] keyArray = keySet.toArray(new String[keySet.size()]);
            Arrays.sort(keyArray);
            StringBuilder sb = new StringBuilder();
            for (String k : keyArray) {
                if (data.get(k) != null && data.get(k).toString().trim().length() > 0) // 参数值为空，则不参与签名
                    sb.append(k).append("=").append(data.get(k).toString().trim()).append("&");
            }
            if (sb.length() > 0) {
                sb.deleteCharAt(sb.length() - 1);
            }
            return hmacSha256(sb.toString(), secretKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        String s = hmacSha256("{\"data\":\"{\"blockHash\":\"0x95956962812bdfabd85bc23044cd65e2094fc0791b7a39c942ca92973441907f\",\"isMemo\":0,\"symbol\":\"ETH\",\"toAddr\":\"0xea7ea5b7bad0bfd8b89faf929807104db193aa2c\",\"amount\":0.500000000000,\"addressType\":1,\"billType\":100,\"notifyStatus\":1,\"txid\":\"0x1979673c57dd5b985fa7ba5b251f870f3f9db13f442d25193b7442082e6ac652\",\"confirmations\":7,\"sid\":\"M_b23806bd4ab7974f9d79e23e499159b3c9466c94e2ad3c474735acda41824d55\",\"coinId\":2,\"blockHeight\":11319896,\"chainId\":2,\"dType\":1,\"txFee\":0E-12,\"ctime\":1636081647174,\"id\":\"c0c3a\",\"fromAddr\":\"0xa02bc38012f16fa74abcf5df2c3940657c25261b\",\"status\":10}\"}",
                "03e3c82f4eb5f15cc06bf2d7ce3c6ccbc32cb01eca93b22b5ba9017c58798a8d");
        System.out.println(s);
    }


    public static String hmacSha256(String value,String key){
        String result = null;
        byte[] keyBytes = key.getBytes();
        SecretKeySpec localMac = new SecretKeySpec(keyBytes, "HmacSHA256");
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(localMac);
            byte[] arrayOfByte = mac.doFinal(value.getBytes());
            BigInteger localBigInteger = new BigInteger(1,
                    arrayOfByte);
            result = String.format("%0" + (arrayOfByte.length << 1) + "x",
                    new Object[] { localBigInteger });

        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        return result;
    }
}
