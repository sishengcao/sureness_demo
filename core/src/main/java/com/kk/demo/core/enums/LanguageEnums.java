package com.kk.demo.core.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import com.kk.demo.core.exceptions.BusinessException;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 语言类型
 */
@Getter
public enum LanguageEnums {

    /*简体中文*/
    //CHINE_NEW("zh-rCN", Arrays.asList("86","886","853","852")),
    /*繁体中文*/
    CHINE_OLD("zh-rHK", Arrays.asList("886","853","852")),
    /*英语*/
    ENGLISH("en"),
    /*日语*/
    JAPANESE("ja", Arrays.asList("81")),
    /*泰语*/
    THAI("th", Arrays.asList("66")),
    /**
     * 阿拉伯
     */
    AR("ar"),
    /**
     * 朝鲜文/韩文
     */
    KO("ko",Arrays.asList("82")),
    /**
     * 俄语
      */
    RU("ru",Arrays.asList("7")),
    /**
     * 印尼
     */
    ID("id",Arrays.asList())
    ;


    @EnumValue
    private final String value;

    //手机区号
    private List<String> phoneArea;

    LanguageEnums(String value) {
        this.value = value;
    }

    LanguageEnums(String value, List<String> phoneArea) {
        this.value = value;
        this.phoneArea = phoneArea;
    }

    @JsonValue
    public String getValue() {
        return this.value;
    }

    public static LanguageEnums fromName(String name) {
        if (null == name) {
            return null;
        }
        for (LanguageEnums b : LanguageEnums.values()) {
            if (b.value.equalsIgnoreCase(name)) {
                return b;
            }
        }
        throw new BusinessException("不存在这种语言类型:" + name);
    }

    public static List<String> getValueList() {
        List<String> keyList = new ArrayList<>();
        for (LanguageEnums value : LanguageEnums.values()) {
            keyList.add(value.getValue());
        }
        return keyList;
    }

    public static LanguageEnums getByPhoneArea(String phoneArea) {
        if (StringUtils.isBlank(phoneArea)) {
            return LanguageEnums.CHINE_OLD;
        }
        for (LanguageEnums value : LanguageEnums.values()) {
            if (value.phoneArea != null && value.phoneArea.contains(phoneArea)) {
                return value;
            }
        }
        return null;
    }
}
