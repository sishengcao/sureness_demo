package com.kk.demo.core.utils;

/**
 * @author: ziHeng
 * @description:
 * @create: 2020/06/15 20:31
 */
public class PasswordUtils {

    private final static String salt = "asdfwqqeczxcd";


    //获取盐值密码
    public static String getSaltPassword(String password) {
        return MD5Utils.MD5Encode(password.toLowerCase() + salt, "UTF-8");
    }

    /**
     *
     * @param password 前端传入的md5密码
     * @param saltPassword 数据库存的加盐md5密码
     * @return
     */
    //是否同一密码
    public static boolean isSamePassword(String password, String saltPassword) {
        String saltPassword1 = getSaltPassword(password);
        return saltPassword1.equals(saltPassword);
    }

}
