package com.kk.demo.core.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.math.BigDecimal;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/07/14 18:29
 */
@JsonComponent
public class JsonConfig {


    @Bean
    public ObjectMapper jacksonObjectMapper(Jackson2ObjectMapperBuilder builder) {
        ObjectMapper objectMapper = builder.createXmlMapper(false).build();
        /**
        * 可配置项：
        * 1.Include.Include.ALWAYS : 默认
        * 2.Include.NON_DEFAULT : 默认值不序列化
        * 3.Include.NON_EMPTY : 属性为 空（""） 或者为 NULL 都不序列化
        * 4.Include.NON_NULL : 属性为NULL 不序列化
        **/
        // objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        /**
        * 序列化时,将所有的long变成string
        */
        SimpleModule module = new SimpleModule();
        module.addSerializer(BigDecimal.class, ToStringSerializer.instance);
        module.addSerializer(BigDecimal.class, BigDecimalToStringSerializer.instance);
        objectMapper.registerModule(module);
        return objectMapper;
    }

}
