package com.kk.demo.core.exceptions;

import com.kk.demo.core.response.ApiResponse;

/**
 * @author AaronXu
 * @date 2020/7/11 10:10
 * 非法参数异常
 */
public class IllegalParameterException extends RuntimeException {
    private static final long serialVersionUID = 8956241560115L;
    protected Integer code;
    protected String message;
    private Object replacer;

    public IllegalParameterException(ApiResponse.Status apiResponse) {
        this(apiResponse.getCode(), apiResponse.getStandardMessage());
    }

    public IllegalParameterException(ApiResponse.Status apiResponse, String message) {
        this(apiResponse.getCode(), apiResponse.getStandardMessage() + message);
    }

    public IllegalParameterException(String msg) {
        this(4444, msg);
    }

    public IllegalParameterException(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getReplacer() {
        return replacer;
    }

    public void setReplacer(Object replacer) {
        this.replacer = replacer;
    }
}
