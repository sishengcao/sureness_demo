package com.kk.demo.core.enums.message;


import lombok.Getter;

import java.util.Objects;

@Getter
public enum SystemMsgTypeEnum {
    /**
     * 系统公告
     */
    SYS_NOTICE(1, "SYS_NOTICE", "系统公告"),

    MENTION_CURRENCY_SUCCEED(2, "MENTION_CURRENCY_SUCCEED", "提币成功通知"),
    MENTION_CURRENCY_DEFEATED(3, "MENTION_CURRENCY_DEFEATED", "提币失败通知"),
    RECHARGE_CURRENCY_SUCCEED(4, "RECHARGE_CURRENCY_SUCCEED", "充币成功通知"),
    RECHARGE_CURRENCY_DEFEATED(5, "RECHARGE_CURRENCY_DEFEATED", "充币失败通知"),


    AGENT_LEVEL(13, "AGENT_LEVEL", "代理等级变更通知(手动)"),
    AGENT_LEVEL_AUTO(14, "AGENT_LEVEL_AUTO", "代理等级变更通知(自动)"),
    AGENT_LEVEL_UP_AUDIT_DEFEATED(16, "AGENT_LEVEL_UP_AUDIT_DEFEATED", "代理审核不通过通知"),
    ORDER_PLACE_SUCCESS(21,"ORDER_PLACE_SUCCESS","下单成功通知"),
    ORDER_BE_ABOUT_TO_EXPIRE(22, "ORDER_BE_ABOUT_TO_EXPIRE", "订单即将到期通知"),
    ORDER_EXPIRE(23, "ORDER_EXPIRE", "订单到期通知"),
    ORDER_POWER_EXPIRE(24, "ORDER_POWER_EXPIRE", "电费到期通知"),
    ORDER_FREEZE(25, "ORDER_FREEZE", "订单冻结通知"),
    ORDER_POSTPONE(26, "ORDER_POSTPONE", "订单延期通知"),
    ORDER_TERMINATION(27, "ORDER_TERMINATION", "订单终止通知"),

    ;

    private Integer key;
    private String code;
    private String value;

    SystemMsgTypeEnum(Integer key, String code, String value) {
        this.key = key;
        this.code = code;
        this.value = value;
    }

    public static String getCodeByKey(int key) {
        for (SystemMsgTypeEnum value : SystemMsgTypeEnum.values()) {
            if (Objects.equals(key, value.getKey())) {
                return value.getCode();
            }
        }
        return "";
    }

    public static Integer getKeyByCode(String code) {
        for (SystemMsgTypeEnum value : SystemMsgTypeEnum.values()) {
            if (Objects.equals(code, value.getCode())) {
                return value.getKey();
            }
        }
        return null;
    }

    public static String getValueByCode(String code) {
        for (SystemMsgTypeEnum enums : SystemMsgTypeEnum.values()) {
            if (enums.getCode().equals(code)) {
                return enums.getValue();
            }
        }
        return null;
    }


}
