package com.kk.demo.core.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author AaronXu
 * @date 2021/8/12 10:30
 */
public class CollectionUtils {

    /**
     * 校验集合为空
     */
    public static boolean isEmpty(List<?> list) {
        return org.springframework.util.CollectionUtils.isEmpty(list);
    }

    /**
     * 校验集合不为空
     */
    public static boolean isNotEmpty(List<?> list) {
        return !isEmpty(list);
    }

    /**
     * 返回空集合
     */
    public static <T> List<T> getEmptyList() {
        return Collections.emptyList();
    }

}
