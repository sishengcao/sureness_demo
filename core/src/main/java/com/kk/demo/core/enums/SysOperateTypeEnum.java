package com.kk.demo.core.enums;

/**
 * 操作类型
 */
public enum SysOperateTypeEnum {
    /**
     * 其它
     */
    OTHER,

    /**
     * 查询
     */
    QUERY,

    /**
     * 新增
     */
    INSERT,

    /**
     * 修改
     */
    UPDATE,

    /**
     * 删除
     */
    DELETE,

    /**
     * 授权
     */
    GRANT,

    /**
     * 导出
     */
    EXPORT,

    /**
     * 导入
     */
    IMPORT
}
