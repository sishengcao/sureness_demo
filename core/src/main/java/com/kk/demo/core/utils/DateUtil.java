package com.kk.demo.core.utils;

import org.apache.commons.lang.time.DateFormatUtils;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateUtil {

    public static final String DATE_PARTTEN = "yyyy-MM-dd";
    public static final String TIME_PARTTEN = "HH:mm:ss";
    public static final String DAYTIME_PARTTEN = "yyyy-MM-dd HH:mm:ss";
    public static final String MILLIONSECONDS_PARTTEN = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String DAYTIME_PARTTEN2 = "yyyyMMddHHmmss";

    public enum DateTypeEnums {

        DAY("day",Calendar.DATE),
        MONTH("month",Calendar.MONTH),
        YEAR("year",Calendar.YEAR),
        HOUR("hour",Calendar.HOUR)
        ;


        private String dateTypeValue;

        private Integer calendarValue;

        DateTypeEnums(String dateTypeValue, Integer calendarValue) {
            this.dateTypeValue = dateTypeValue;
            this.calendarValue = calendarValue;
        }
    }

    public static Date dateAdd(Date date, DateTypeEnums dateType, Integer count) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(dateType.calendarValue, count);
        return cal.getTime();
    }

    public static int getDaysOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }


    public static Integer tianChaZhi(Date start, Date end) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = end.getTime() - start.getTime();
        // 计算差多少天
        Long day = diff / nd;
        Integer shijiancha = Integer.parseInt(day.toString());
        return shijiancha;
    }

    /**
     * 判断年月日是否相等
     * @param start
     * @param end
     * @return start >= end
     */
    public static boolean equitAndLast(Date start, Date end) {
        start = setSfmZero(start);
        end = setSfmZero(end);
        // 获得两个时间的毫秒时间差异
        long diff = end.getTime() - start.getTime();
        if(diff <= 0){
            return true;
        }
        return false;
    }


    public static int daysOfTwo(Date fDate, Date oDate) {
        // 安全检查
        if (fDate == null || oDate == null) {
            throw new IllegalArgumentException("date is null, check it again");
        }
        // 根据相差的毫秒数计算
        int days = (int) ((oDate.getTime() - fDate.getTime()) / (24 * 3600 * 1000));
        return days;
    }

    public static String zhunqueChazhi(Date start, Date end) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = end.getTime() - start.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        long sec = diff % nd % nh % nm / ns;

        long hm = diff % nd % nh % nm;
        return day + "天" + hour + "小时" + min + "分钟" + sec + "秒" + hm + "毫秒";
    }

    //获取今天0点0分
    public static Date getZeroDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date zero = calendar.getTime();
        return zero;
    }

    public static Date getZeroDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date zero = calendar.getTime();
        return zero;
    }

    public static Date setRiqiZero(Date date, Integer riqi) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DATE, riqi);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date date2 = calendar.getTime();
        return date2;
    }

    public static Date setSfmZero(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        //设置时分秒毫秒为0
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date setTimeLast(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        //设置时分秒毫秒为0
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 获取某个时间的年月日
     */
    public static String getNyrString(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_PARTTEN);
        return simpleDateFormat.format(date);
    }

    public static Date getNextMonthOneRi(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.MONTH, 1);
        //设置时分秒为0
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * @date: 2020/11/10 9:48
     * @Author: AaronXu
     * @Description: 求得从某天开始，过了几年几月几日几时几分几秒后，日期是多少 几年几月几日几时几分几秒可以为负数
     */
    public static Date modifyDate(Date date, int year, int month, int day, int hour, int min, int sec) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, year);
        cal.add(Calendar.MONTH, month);
        cal.add(Calendar.DATE, day);
        cal.add(Calendar.HOUR, hour);
        cal.add(Calendar.MINUTE, min);
        cal.add(Calendar.SECOND, sec);

        return cal.getTime();
    }

    /**
     * @date: 2020/11/10 9:50
     * @Author: AaronXu
     * @Description: (n > 0)获取N天前 (n<0)获取N天后    零时时间,2020 01:01 00:00:00
     */
    public static String getLastZeroTime(int n) {
        SimpleDateFormat df = new SimpleDateFormat(DATE_PARTTEN);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -n);
        return df.format(calendar.getTime()) + " 00:00:00";
    }

    /**
     * @date: 2020/11/10 9:50
     * @Author: AaronXu
     * @Description: (n > 0)获取N天前 (n<0)获取N天后  2020 01:01 23:59:59
     */
    public static String getLastThreeTime(int n) {
        SimpleDateFormat df = new SimpleDateFormat(DATE_PARTTEN);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -n);
        return df.format(calendar.getTime()) + " 23:59:59";
    }

    public static int getDays(Date begin, Date end) {
        long time = end.getTime() - begin.getTime();
        int n = (int) (time / (1000 * 24 * 3600));
        return n;
    }


    /**
     * 获取开始与结束时间之间的日期列表
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static List<Long> getBetweenDateList(Date startDate, Date endDate) {
        Date tmpDate = new Date();
        tmpDate.setTime(startDate.getTime());
        List<Long> dateList = new ArrayList<>();
        while (tmpDate.compareTo(endDate) != 1) {
            dateList.add(getZeroDate(tmpDate).getTime());
            tmpDate.setTime(tmpDate.getTime() + 1000 * 60 * 60 * 24);
        }
        return dateList;
    }

    public static final String d2s(Date d, String mask) {
        SimpleDateFormat sdf = new SimpleDateFormat(mask);
        String rString = sdf.format(d);
        return rString;
    }

    /**
     * 按指定格式转化成字符串
     *
     * @param date
     * @param pattern
     * @return
     */
    public static String formatToStr(Date date, String pattern) {
        return DateFormatUtils.format(date, pattern);
    }

    /**
     * 获取某个时间的年份
     */
    public static String getFormatYear(Date date) {
        return DateFormatUtils.format(date, "yyyy");
    }

    /**
     * 获取某个时间的年月
     */
    public static String getFormatYearMonth(Date date) {
        return DateFormatUtils.format(date, "yyyy-MM");
    }

    /**
     * 获取某个时间的年月日
     */
    public static String getFormatYearMonthDay(Date date) {
        return DateFormatUtils.format(date, "yyyy-MM-dd");
    }

    /**
     * 将时间字符转转成时间格式
     */
    public static Date strToDate(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        ParsePosition pos = new ParsePosition(0);
        Date strtodate = formatter.parse(strDate, pos);
        return strtodate;
    }

    /**
     * 将时间字符转转成时间格式
     */
    public static Date strToDate(String strDate, String str) {
        SimpleDateFormat formatter = new SimpleDateFormat(str);
        ParsePosition pos = new ParsePosition(0);
        Date strtodate = formatter.parse(strDate, pos);
        return strtodate;
    }

    /**
     * @description 判断某个时间点是否在某个时间范围内
     * @param nowTime 时间点
     * @param beginTime 时间范围头
     * @param endTime 时间范围尾
     * @return 在：true; 不在：false
     */
    public static boolean containCalendar(Date nowTime, Date beginTime, Date endTime) {
        Calendar now = Calendar.getInstance();
        now.setTime(nowTime);
        Calendar begin = Calendar.getInstance();
        begin.setTime(beginTime);
        Calendar end = Calendar.getInstance();
        end.setTime(endTime);
        if (now.after(begin) && now.before(end)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @description 判断某个时间点是否在某个时间范围内
     * @param date 时间点
     * @param startDate 开始时间
     * @param endDate 结束时间
     * @return 在：true; 不在：false
     * 包头不包尾
     * 另外：date = startDate: true
     * date == endDate: false
     *
     */
    public static boolean isBetweenTwoDate(Date date, Date startDate, Date endDate) {
        return startDate.before(date) && endDate.after(date);
    }

    /**
     *
     * @param now 当前时间
     * @param checkDate 比较时间
     *
     * @return true: 当前时间 > 比较时间  false:当前时间 <= 比较时间
     */
    public static boolean afterDay(Date now, Date checkDate) {
        if(DateUtil.setSfmZero(checkDate).getTime() - DateUtil.setSfmZero(now).getTime() <= 0){
            return false;
        }
        return true;
    }


    public static void main(String[] args) {
        Date now = new Date();
        Date start = DateUtil.strToDate("2021-11-14 23:32:22");
        Date end = DateUtil.strToDate("2021-11-15");
        System.out.println(getDays(setSfmZero(now),setSfmZero(start)));
    }


}
