package com.kk.demo.core.utils;

import com.alibaba.fastjson.JSON;

import java.util.List;
import java.util.Map;

/**
 * @USER: beisheng
 * @DATE: 2021/7/3 11:43
 */
public class ProductUtil {

    //检测group唯一性
    public static Boolean checkIncomeGroupSole(List<String> codeList) {
        if (codeList.contains("UNITE_EXTENT") && codeList.contains("UNITE_BACK")) return false;
        return true;
    }

    //检测BlendSchemaType
    public static Integer getBlendSchemaType(List<String> incomeIds) {
        //类型:1联合,2线性,3联合+线性
        Integer blendSchemaType = 0;
        if ((incomeIds.contains("UNITE_EXTENT") && !incomeIds.contains("RELEASE")) || (incomeIds.contains("UNITE_BACK") && !incomeIds.contains("RELEASE"))) {
            blendSchemaType = 1;
        } else if (incomeIds.contains("RELEASE")) {
            blendSchemaType = 2;
        } else if ((incomeIds.contains("UNITE_EXTENT") && incomeIds.contains("RELEASE")) || (incomeIds.contains("UNITE_BACK") && incomeIds.contains("RELEASE"))) {
            blendSchemaType = 3;
        }
        return blendSchemaType;
    }

    public static Map<String, Object> copyMap(Map map) {
        return JSON.parseObject(JSON.toJSONString(map));
    }

    public static <T> List<T> copyList(List list, Class<T> tClass) {
        if (CollectionUtils.isEmpty(list)) {
            return CollectionUtils.getEmptyList();
        }
        return JSON.parseArray(JSON.toJSONString(list), tClass);
    }

}
