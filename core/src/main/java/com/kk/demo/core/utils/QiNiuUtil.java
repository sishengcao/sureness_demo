package com.kk.demo.core.utils;

import com.kk.demo.core.config.QiNiuConfig;
import com.qiniu.util.Auth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/07/23 10:59
 */
@Component
public class QiNiuUtil {


    @Autowired
    private QiNiuConfig qiNiuConfig;

    public String getToken(){
        Auth auth = Auth.create(qiNiuConfig.getAccessKeyId(),qiNiuConfig.getSecretKey());
        return auth.uploadToken(qiNiuConfig.getBucket());
    }


}
