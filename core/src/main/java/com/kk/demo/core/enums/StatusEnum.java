package com.kk.demo.core.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;


@Getter
public enum StatusEnum {

    BLOCK_WAIT(3,"冻结等待"),
    UN_CHECK(2,"未处理"),
    Y(1, "正常/是"),
    N(0, "异常/否");

    @EnumValue
    private final Integer status;

    private final String value;

    StatusEnum(int status, String name) {
        this.status = status;
        this.value = name;
    }

    @JsonValue
    public int getStatus() {
        return status;
    }

    public static StatusEnum getByStatus(Integer status){
        for (StatusEnum statusEnum : StatusEnum.values()) {
            if(statusEnum.getStatus()==status){
                return statusEnum;
            }
        }
        return null;
    }
}
