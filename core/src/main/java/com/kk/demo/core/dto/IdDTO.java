package com.kk.demo.core.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @ClassName IdDTO
 * @Description 业务idDTO
 * @Author July
 * @Date: 2021-07-19 18:15:04
 **/
@Data
public class IdDTO {
    private String language;
    @ApiModelProperty("业务id")
    private Integer id;
}
