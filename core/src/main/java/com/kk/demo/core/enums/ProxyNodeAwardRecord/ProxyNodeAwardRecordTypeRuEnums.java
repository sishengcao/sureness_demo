package com.kk.demo.core.enums.ProxyNodeAwardRecord;

import lombok.Getter;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/08/29 14:22
 */
@Getter
public enum ProxyNodeAwardRecordTypeRuEnums {

    COMMISSION(1, "Обычные ноды – отчисление доли прибыли"),
    CENT(2, "Обычные ноды – монеты"),
    SUPER_NODE_COMISSION(3, "Суперноды – отчисление доли прибыли"),
    SUPER_NODE_CENT(4, "Суперноды – монеты"),


    COMMISSION_PEERS(10, "Обычные ноды – отчисление доли прибыли (одинаковых уровней)"),
    CENT_PEERS(20, "Обычные ноды – монеты (одинаковых уровней)"),
    SUPER_NODE_COMISSION_PEERS(30, "Суперноды – отчисление доли прибыли (одинаковых уровней)"),
    SUPER_NODE_CENT_PEERS(40, "Суперноды – монеты (одинаковых уровней)");

    private Integer type;

    private String name;

    ProxyNodeAwardRecordTypeRuEnums(Integer type, String name) {
        this.type = type;
        this.name = name;
    }
}
