package com.kk.demo.core.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberUtil {
    private static final long EPOCH = 1479533469598L; //开始时间,固定一个小于当前时间的毫秒数
    private static final int max12bit = 4095;
    private static final long max41bit = 1099511627775L;
    private static String machineId = ""; // 机器ID

    public static String toAsset = "toAsset";

    public static String pledgeToAsset = "pledgeToAsset";

    public static String gasToAsset = "gasToAsset";

    public static String toPledgeAsset = "toPledgeAsset";

    public static String toGasAsset = "toGasAsset";

    //充币
    public static String depositCreate ="depositCreate";

    //充币审核成功
    public static String depositAuditSuccess = "depositAuditSuccess";

    //提币审核成功
    public static String withdrawalAuditSuccess = "withdrawalAuditSuccess";

    //业内转账
    public static String moneyTransferToUser = "moneyTransferToUser";

    public static String order="order";

    public static String getBonus = "airBonus";

    //提币创建
    public static String withdrawalCreate="withdrawalCreate";

    //系统充值
    public static String systemDepositCreate = "systemDepositCreate";

    //系统扣除
    public static String systemWithdrawalCreate = "systemWithdrawalCreate";

    //提成收益
    public static String rateProfitAward = "rateProfitAward";

    //资产类型划转审核
    public static String checkoutAssetTransferAudit ="checkoutAssetTransferAudit";

    //duoduo钱包
    public static String duoduobitTopUp="duoBankTopUp";

    public static String duoduobitWithDraw="duoduobitWithDraw";


    //eToken转入
    public static String transferFromEToken="transferFromEToken";

    public static String transferToEToken="transferToEToken";

    //续缴电费
    public static String renewalOccupied="renewalOccupied";

    /**
     * 创建流水号
     */
    public static String create(String prex) {

        long time = System.currentTimeMillis() - EPOCH + max41bit;
        // 二进制的 毫秒级时间戳
        String base = Long.toBinaryString(time);

        // 序列数
        String randomStr = StringUtils.leftPad(Integer.toBinaryString(new Random().nextInt(max12bit)), 12, '0');
        if (StringUtils.isNotEmpty(machineId)) {
            machineId = StringUtils.leftPad(machineId, 10, '0');
        }

        //拼接
        String appendStr = base + machineId + randomStr;
        // 转化为十进制 返回
        BigInteger bi = new BigInteger(appendStr, 2);

        return prex + Long.valueOf(bi.toString()) + RandomStringUtils.randomAlphabetic(3);
    }

    public static Integer getNumberFromString(String str) {
        String regEx = "[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return Integer.valueOf(m.replaceAll("").trim());
    }

    /**
     * 隐藏中间4位数
     *
     * @param number
     * @return
     */
    public static String hide4MiddleNumber(String number) {
        if (StringUtils.isBlank(number)) {
            return number;
        }
        if (number.length() > 4) {
            int hideLength = number.length() - 4;
            int firstLength = hideLength / 2;
            int lastLength = hideLength - firstLength;
            return number.replaceAll("(\\w{" + firstLength + "})\\w{4}(\\w{" + lastLength + "})", "$1****$2");
        }
        return number.replaceAll("(\\w)", "*");
    }

    public static String hideEmail(String email) {
        if (StringUtils.isBlank(email)) {
            return email;
        }
        return email.replaceAll("(\\w?)(\\w+)(\\w)(@\\w+\\.[a-z]+(\\.[a-z]+)?)",
                "$1****$3$4");
    }

}
