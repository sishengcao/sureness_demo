package com.kk.demo.core.config;


import com.kk.demo.core.enums.ApiTokenTypeEnums;

/**
 * @author: ziHeng
 * @description: 各端api token属性  此处各端api入口需要实现
 * @create: 2020/12/25 16:15
 */
public interface ApiConfigSetting {

    ApiTokenTypeEnums getApiTypeEnums();


}
