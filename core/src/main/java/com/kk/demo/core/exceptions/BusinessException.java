package com.kk.demo.core.exceptions;


import com.kk.demo.core.response.ApiResponse;

public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = 9841241560115L;
    protected Integer code;
    protected String message;
    private Object replacer;

    public BusinessException(ApiResponse.Status apiResponse) {
        this(apiResponse.getCode(), apiResponse.getStandardMessage());
    }

    public BusinessException(ApiResponse.Status apiResponse,String message) {
        this(apiResponse.getCode(), message);
    }

    public BusinessException(String message) {
        this(ApiResponse.Status.SYSTEM_FAILED.getCode(), message);
    }

    public BusinessException(Integer code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getReplacer() {
        return replacer;
    }

    public void setReplacer(Object replacer) {
        this.replacer = replacer;
    }
}
