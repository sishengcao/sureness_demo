package com.kk.demo.core.utils;

import com.kk.demo.core.enums.HeaderEnums;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/08/03 17:07
 */
@Component
public class HttpServletUtil {

    //线程安全
    @Autowired
    private HttpServletRequest request;

    public String getLanguage() {
        String language = request.getHeader(HeaderEnums.LANGUAGE.getHeaderName());
        if (StringUtils.isBlank(language)) {
            language = "zh-rCN";
        }

        return language;
    }

    public String getVisitMethod() {
        String language = request.getHeader(HeaderEnums.VISIT_METHOD.getHeaderName());
        if (StringUtils.isBlank(language)) {
            language = "UNKNOWN";
        }

        return language;
    }

    public String getVisitMethod(HttpServletRequest theRequest) {
        String language = theRequest.getHeader(HeaderEnums.VISIT_METHOD.getHeaderName());
        if (StringUtils.isBlank(language)) {
            language = "UNKNOWN";
        }

        return language;
    }

    public HttpServletRequest getRequest(){
        return request;
    }
}
