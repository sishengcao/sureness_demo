package com.kk.demo.core.utils.parameterCheck;

import java.lang.annotation.*;

/**
 * @date: 2021/6/9 11:04
 * @Author: AaronXu
 * @Description: 不能校验有继承关系的父类字段,父类的字段无法校验
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Check {
    //校验字段名(字段描述)
    String comment() default "";

    // 非空校验
    boolean notNull() default false;

    // 最大长度校验
    int maxLen() default 8;

    // 最小长度校验
    int minLen() default 1;

    // 整数校验
    boolean numeric() default false;

    // 小数校验(8位)
    boolean decimal() default false;

    // 邮箱格式校验
    boolean mail() default false;

    // 固定电话格式校验
    boolean phone() default false;

    // 移动电话格式校验
    boolean mobile() default false;

    // IP地址校验
    boolean ip() default false;

    // URL校验
    boolean URL() default false;

    // 密码校验
    boolean password() default false;

}
