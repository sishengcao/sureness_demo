package com.kk.demo.core.enums;

/**
 * 平台日志操作类型枚举
 */
public enum SysOpenModuleEnum {
    OTHER("其他"),
    ADD_UPDATE_PRODUCT_TYPE("产品类型"),
    ADD_UPDATE_PRODUCT("产品"),
    ;
    private final String moduleName;

    SysOpenModuleEnum(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleName() {
        return moduleName;
    }
}
