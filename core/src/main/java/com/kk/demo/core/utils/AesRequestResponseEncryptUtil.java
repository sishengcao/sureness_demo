package com.kk.demo.core.utils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * 请求响应的加解密 使用填充方式
 */
public class AesRequestResponseEncryptUtil {
    private final static Logger logger = LoggerFactory.getLogger(AesRequestResponseEncryptUtil.class);

    //private static final String TAG = "AesEncryptUtil";
    private static final String UTF8 = "UTF-8";
    private static final String AES = "AES";
    private static final String AES_CBC_PKCS5_PADDING = "AES/CBC/PKCS5Padding";
    //private static final String AES_CBC_NO_PADDING = "AES/CBC/NoPadding";


    private static final String aesKey = "64sfe88aec1b0d57";
    private static final String aesIV = "c709e1bcb45362hg";

    /**
     * JDK只支持AES-128加密，也就是密钥长度必须是128bit；参数为密钥key，key的长度小于16字符时用"0"补充，key长度大于16字符时截取前16位
     **/
    private static SecretKeySpec create128BitsKey(String key) {
        if (key == null) {
            key = "";
        }
        byte[] data = null;
        StringBuffer buffer = new StringBuffer(16);
        buffer.append(key);
        //小于16后面补0
        while (buffer.length() < 16) {
            buffer.append("0");
        }
        //大于16，截取前16个字符
        if (buffer.length() > 16) {
            buffer.setLength(16);
        }
        try {
            data = buffer.toString().getBytes(UTF8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new SecretKeySpec(data, AES);
    }

    /**
     * 创建128位的偏移量，iv的长度小于16时后面补0，大于16，截取前16个字符;
     *
     * @param iv
     * @return
     */
    private static IvParameterSpec create128BitsIV(String iv) {
        if (iv == null) {
            iv = "";
        }
        byte[] data = null;
        StringBuffer buffer = new StringBuffer(16);
        buffer.append(iv);
        while (buffer.length() < 16) {
            buffer.append("0");
        }
        if (buffer.length() > 16) {
            buffer.setLength(16);
        }
        try {
            data = buffer.toString().getBytes(UTF8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new IvParameterSpec(data);
    }

    /**
     * 填充方式为Pkcs5Padding时，最后一个块需要填充χ个字节，填充的值就是χ，也就是填充内容由JDK确定
     *
     * @param srcContent
     * @param password
     * @param iv
     * @return
     */
    private static byte[] aesCbcPkcs5PaddingEncrypt(byte[] srcContent, String password, String iv) {
        SecretKeySpec key = create128BitsKey(password);
        IvParameterSpec ivParameterSpec = create128BitsIV(iv);
        try {
            Cipher cipher = Cipher.getInstance(AES_CBC_PKCS5_PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, key, ivParameterSpec);
            byte[] encryptedContent = cipher.doFinal(srcContent);
            return encryptedContent;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private static byte[] aesCbcPkcs5PaddingDecrypt(byte[] encryptedContent, String password, String iv) {
        SecretKeySpec key = create128BitsKey(password);
        IvParameterSpec ivParameterSpec = create128BitsIV(iv);
        try {
            Cipher cipher = Cipher.getInstance(AES_CBC_PKCS5_PADDING);
            cipher.init(Cipher.DECRYPT_MODE, key, ivParameterSpec);
            byte[] decryptedContent = cipher.doFinal(encryptedContent);
            return decryptedContent;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String decrypt(String encryptText) {
        if(StringUtils.isBlank(encryptText)){
            return "";
        }
        try {
            encryptText = encryptText.trim();
            byte[] encryptByte = encryptText.getBytes();
//            byte[] decryptByte = Base64.getDecoder().decode(encryptByte);
            byte[] decryptByte = Base64.getMimeDecoder().decode(encryptByte);
            byte[] decryptTextByte = aesCbcPkcs5PaddingDecrypt(decryptByte, aesKey, aesIV);
            String decryptText = new String(decryptTextByte);
            return decryptText.trim();
        } catch (Exception e) {
            logger.error("decrypt error: ", e);
            return null;
        }
    }

    public static String encrypt(String descryptText) {
        if(StringUtils.isBlank(descryptText)){
            return "";
        }
        try {
            descryptText = descryptText.trim();
            byte[] encryptByte = Base64.getEncoder().encode(aesCbcPkcs5PaddingEncrypt(descryptText.getBytes(), aesKey, aesIV));
            String encryptText = new String(encryptByte);
            return encryptText.trim();
        } catch (Exception e) {
            logger.error("encrypt error: ", e);
            return null;
        }
    }


    public static void main(String[] args) {
        String decrypt = decrypt("X8NXVvRQN7ABa9mb7KuTcAZvrYZqQuN4kURmqlFfKEeJ9g4vyiwJwzwelMhdM++iqIIHU5rqsk/VBOjeJTzigT6Zp0kcccCfckP7Rg4Kzm+bqj6XC9OC3v+kc24en71o");
        System.out.println(decrypt);
    }

}
