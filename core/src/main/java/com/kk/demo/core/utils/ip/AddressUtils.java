package com.kk.demo.core.utils.ip;

import com.alibaba.fastjson.JSONObject;
import com.kk.demo.core.config.ProjectConfig;
import com.kk.demo.core.utils.logback.LogUtils;
import net.dongliu.requests.Requests;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.Charset;

/**
 * 获取地址类
 *
 * @author ruoyi
 */
public class AddressUtils {

    // IP地址查询
    public static final String IP_URL = "http://whois.pconline.com.cn/ipJson.jsp";

    // 未知地址
    public static final String UNKNOWN = "未知地址";

    public static String getRealAddressByIP(String ip) {
        String address = UNKNOWN;
        // 内网不查询
        if (IpUtils.internalIp(ip)) {
            return "127.0.0.1";
        }
        if (ProjectConfig.isAddressEnabled()) {
            try {
                String rspStr = Requests.get(IP_URL + "?ip=" + ip + "&json=true").charset(Charset.forName("GBK")).send()
                        .readToText();
                //LogUtils.accessLog.info("getRealAddressByIP resp:[{}]", rspStr);
                if (StringUtils.isEmpty(rspStr)) {
                    LogUtils.systemLog.error("获取地理位置异常 {}", ip);
                    return UNKNOWN;
                }
                JSONObject obj = JSONObject.parseObject(rspStr);
                String region = obj.getString("pro");
                String city = obj.getString("city");
                String country = obj.getString("addr");
                return String.format("%s %s %s", region, city, country);
            } catch (Exception e) {
                LogUtils.systemLog.error("获取地理位置异常 {}", ip);
            }
        }
        return address;
    }
}
