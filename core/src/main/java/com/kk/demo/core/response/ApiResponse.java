package com.kk.demo.core.response;


import com.kk.demo.core.exceptions.BusinessException;

import java.util.Objects;

/**
 * API格式封装
 */
public class ApiResponse {

    //编码
    private int code;

    //消息
    private Object message;

    //数据
    private Object data;

    public ApiResponse(int code, Object message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ApiResponse() {
        this.code = Status.SUCCESS.getCode();
        this.message = Status.SUCCESS.getStandardMessage();
    }

    public int getCode() {
        return code;
    }

    public Object getMessage() {
        return message;
    }

    public Object getData() {
        return data;
    }


    public static <T> ApiResponse ofSuccess(T data) {
//        if (data == null) {
//            return ApiResponse.ofError(Status.BUSINESS_PARAMETER_ERROR);
//        }
        return new ApiResponse(Status.SUCCESS.getCode(), Status.SUCCESS.getStandardMessage(), data);
    }

    public static <T> ApiResponse ofSuccess() {
        return new ApiResponse(Status.SUCCESS.getCode(), Status.SUCCESS.getStandardMessage(), true);
    }

    public static <T> ApiResponse ofError(Status status) {
        return new ApiResponse(status.getCode(), status.getStandardMessage(), null);
    }


    public static <T> ApiResponse ofError(String message) {
        return new ApiResponse(Status.SYSTEM_FAILED.code, message, null);
    }

    public static <T> ApiResponse ofError(Status status, Object data) {
        return new ApiResponse(status.getCode(), data, null);
    }

    public static <T> ApiResponse ofError(BusinessException businessException) {
        return new ApiResponse(businessException.getCode(), businessException.getMessage(), null);
    }

    public static ApiResponse getApiResponse(boolean success) {
        if (success) {
            return ApiResponse.ofSuccess(Status.SUCCESS);
        } else {
            return ApiResponse.ofError(Status.SYSTEM_FAILED);
        }
    }

    public enum Status {

        //系统异常
        BAD_REQUEST(400, "Bad Request","错误请求"),
        UNAUTHORIZED(401, "Unauthorized","无权限"),
        PAYMENT_REQUIRED(402, "Payment Required","Payment Required"),
        FORBIDDEN(403, "Forbidden","未登录"),
        CONFLICT(409, "Conflict","Conflict"),



        SUCCESS(20000, "success", "请求成功"),

        //============  系统异常 5000+  命名规范:system_xxx   =============
        SYSTEM_FAILED(5000, "system_server_error", "服务器异常"),
        SYSTEM_ERROR_TOKEN(5001, "system_error_token", "token失效"),
        SYSTEM_DUPLICATE_RECORD(5002, "system_duplicate_record", "重复记录"),
        SYSTEM_UNAUTHORIZED_USER(5003, "system_unauthorized_user", "越权用户"),
        SYSTEM_NOT_EXIST(5004, "system_not_exist", "无记录"),
        SYSTEM_FREQUENT_OPERATION(5005, "system_frequent_operation", "操作频繁"),
        SYSTEM_ERROR_HEADERS(5006, "system_error_headers", "错误的请求头"),
        SYSTEM_ERROR_SIGN(5007, "system_error_sign", "错误的签名"),
        SYS_EMAIL_TEMPLATE_NOT_EXIST(40068, "sys_email_template_not_exist", "邮箱模板不存在"),
        SMS_ERROR(40069,"sys_sms_error","sms短信发送失败,请检查手机号和区号"),
        SYSTEM_DUPLICATE_OPERATION(40070,"sys_repetition_operation","重复操作"),

        //=================  自定义参数校验 1000+  命名规范:custom_xxx    ===========
        CUSTOM_COULD_NOT_BE_EMPTY(1000, "custom_could_not_be_empty", "不能为空"),
        CUSTOM_LENGTH_TOO_LONG(1001, "custom_length_too_long", "长度超长"),
        CUSTOM_LENGTH_TOO_SHORT(1002, "custom_length_too_short", "长度过短"),
        CUSTOM_ONLY_ALLOW_INTEGERS(1003, "custom_only_allow_integers", "只允许整数"),
        CUSTOM_ERROR_PRECISION(1004, "custom_error_precision", "小数精度有误"),
        CUSTOM_ERROR_EMAIL(1005, "custom_error_email", "无效的邮箱"),
        CUSTOM_ERROR_LANDLINE_NUMBER(1006, "custom_error_landline_number", "无效的座机号码"),
        CUSTOM_ERROR_PHONE_NUMBER(1007, "custom_error_phone_number", "无效的手机号码"),
        CUSTOM_INVALID_IP(1008, "custom_invalid_ip", "非法ip"),
        CUSTOM_INVALID_URL(1009, "custom_invalid_url", "无效的连接"),
        CUSTOM_INVALID_ID_CARD(1010, "custom_invalid_id_card", "无效的身份证号码"),
        CUSTOM_ERROR_PASSWORD_FORMAT(1011, "custom_error_password_format", "密码不符合规定"),
        CUSTOM_INVALID_BANK_NUMBER(1012, "custom_invalid_bank_number", "无效的银行卡"),
        CUSTOM_HUOBI_OUT_AMOUNT_NOT_SUFFICIENT(1013,"CUSTOM_HUOBI_OUT_AMOUNT_NOT_SUFFICIENT","提币数量不足"),
        CUSTOM_GREATER_THAN_OR_EQUAL_TO(1014, "custom_greater_than_or_equal_to", "必须大于等于"),
        MONEY_CHECK_ERROR(1015,"MONEY_CHECK_ERROR","金额校验错误"),

        //=============  用户异常2000+  命名规范:user_xxx    ===============
        USER_ALREADY_APPLY(2001, "user_already_apply", "用户已申请"),
        USER_PAY_PASSWORD_NOT_SET(2002, "user_pay_password_not_set", "未设置支付密码"),
        USER_PAY_PASSWORD_ERROR(2003, "user_pay_password_error", "支付密码有误"),
        USER_LOGIN_EXPIRE(2004, "user_login_expire", "登录异常"),
        USER_NOT_EXIST(2005, "user_not_exist", "用户不存在"),
        USER_NO_PERMISSION(2006, "user_no_permission", "无权限访问"),
        USER_ACCOUNT_FREEZE(2007, "user_account_freeze", "已冻结"),
        USER_NOT_LOGIN(2008, "user_not_login", "登录已失效"),
        USER_ALREADY_EXIST(2009, "user_already_exist", "用户已存在"),
        USER_TYPE_WALLET_NOT_EXIST(2010, "user_type_wallet_not_exist", "用户此类钱包不存在"),
        USER_NOT_SUFFICIENT_FUNDS(2011, "not_sufficient_funds", "余额不足"),
        USER_WITHDRAWAL_NOT_ALLOW(2012, "user_withdrawal_not_allow", "不允许提现"),
        USER_CERTIFICATE_ERROR(2013, "user_certificate_error", "实名失败"),
        USER_EMAIL_NOT_BIND(2014, "user_email_not_bind", "未绑定邮箱"),
        USER_PHONE_NOT_BIND(2015, "user_phone_not_bind", "未绑定手机"),
        USER_PHONE_ALREADY_EXISTS(2016, "user_phone_already_exists", "手机号码已经存在"),
        USER_EMAIL_ALREADY_EXISTS(2017, "user_email_already_exists", "邮箱已经存在"),
        USER_PHONE_ALREADY_BOUND(2018, "user_phone_already_bound", "用户已经绑定了手机"),
        USER_EMAIL_ALREADY_BOUND(2019, "user_email_already_bound", "用户已经绑定了邮箱"),
        USER_CERTIFICATE_ALREADY_EXIST(2020, "user_certificate_already_exist", "该用户已经实名"),
        USER_INVITE_CODE_ERROR(2021, "user_invite_code_error", "邀请码有误"),
        USER_PASSWORD_ERROR(2022, "user_password_or_name_error", "用户名或密码错误"),
        USER_ROLE_ERROR(2023, "user_role_error", "用户角色有误"),
        WALLET_ERROR(2024, "user_assets_error", "钱包系统错误,请重试"),
        USER_FROZEN_MONEY_NOT_SUFFICIENT(2025, "user_frozen_money_not_sufficient", "冻结金额不足"),
        USER_NOT_PROXY(2026,"user_not_proxy","不是代理节点"),
        USER_NOT_CONDITION(2027,"user_not_condition","不满足条件"),
        USER_TOGETHER(2028,"together","禁止相互引用"),
        USER_NICK_NAME_EXIST(2029,"USER_NICK_NAME_EXIST","昵称已存在"),
        USER_GOOGLE_NOT_EXIST(2030,"user_google_not_exist","管理员未设置谷歌验证秘钥,请联系管理员"),
        USER_ASSET_NOT_EXIST(2031,"USER_ASSET_NOT_EXIST","用户币种账号不存在"),
        USER_ASSET_NOT_ALLOW_WITHDRAW(2032,"USER_ASSET_NOT_ALLOW_WITHDRAW","您该币种已经被限制提币, 可联系客服咨询"),

        //==================  订单异常3000+   命名规范:order_xxx   =================
        ORDER_ERROR_ORDER_STATE(3000, "order_error_order_state", "订单状态异常"),
        ORDER_PRICE_CHANGE(3001, "order_price_change", "价格大幅度波动"),
        ORDER_PRICE_ERROR(3002, "order_price_error", "订单金额错误"),
        ORDER_DISCOUNT_USED(3003, "order_discount_use", "该优惠券已使用"),
        ORDER_NOT_DISCOUNT_CONDITION(3004, "order_not_discount_condition", "未达到满减条件"),
        ORDER_NOT_FOUND(3005, "order_not_found", "订单不存在"),
        ORDER_COUPON_NOT_FOUND(3006, "order_coupon_not_found", "优惠券不存在"),
        ORDER_LESS_THAN_MIN_PURCHASE_NUM(3007,"order_less_than_min_purchase_num","购买未超过起购数量"),
        ORDER_GREATER_THAN_MAX_PURCHASE_NUM(3008,"order_less_than_min_purchase_num","购买超过限购"),
        ORDER_PURCHASE_NUM_NOT_ENOUGH(3009, "order_purchase_num_not_enough", "少于最低购买数量"),
        ORDER_SAVE_ERROR(3010, "ORDER_SAVE_ERROR", "订单保存异常"),


        //=================   产品异常4000+ 命名规范:product_xxx    =================
        PRODUCT_TYPE_NOT_EXIST(4000, "product_type_not_exist", "产品类别不存在"),
        PRODUCT_TYPE_EXIST(4001, "product_not_exist", "产品不存在"),
        PRODUCT_OCCUPIED_PRICE_NO_SET(4002, "product_occupied_price_no_set", "机位费价格未设置"),
        PRODUCT_SKU_ERROR(4003, "product_sku_error", "sku异常"),
        PRODUCT_STORE_NOT_SUFFICIENT(4004, "product_store_not_sufficient", "库存不足"),
        PRODUCT_STORE_CHANGE(4005, "product_store_change", "库存发生变化，请重试"),
        SELL_CANNOT_BIGGER_THAN_STORE(4006, "product_store_cannot_bigger_than_store", "商品设置已售不能大于库存"),
        PRODUCT_CHINA_NAME_NOT_SETTING(4007, "product_china_name_not_setting", "中文名字未设置"),
        PRODUCT_REPEAT_SKU(4008,"product_repeat_sku","产品重复的sku"),
        OCCUPIED_PRICE_ALL_DAY_ONLY_ONE(4009,"occupied_price_all_day_only_one","全缴只能设置一个"),
        PRODUCT_BACK_COUNTS_MUST_EXIST(4010,"back_counts_must_exist","返还数量不能为空"),
        PRODUCT_BACK_CONTENT_MUST_EXIST(4011,"back_content_must_exist","空投文本不能为空"),

        //=====================  公共异常6000+ 命名规范:common_xxx    ================
        COMMON_COUPON_COUNT_NOT(6000, "common_coupon_count_not", "优惠券使用次数已上限"),
        COMMON_COUPON_COUNT_MAX(6001, "common_coupon_count_max", "领取优惠券数量过多"),
        COMMON_COUPON_LOSE(6002, "common_coupon_lose", "优惠券已过期"),
        COMMON_COUPON_RECEIVE_UPPER_LIMIT(6003, "common_receive_upper_limit", "优惠券已领完"),
        COMMON_COUPON_GET_AMOUNT_IS_LIMIT(6004, "common_get_amount_is_limit", "优惠限制数量有误"),
        COMMON_STATUS_ERROR(6005, "common_status_error", "状态错误"),
        COMMON_NOT_FOUND(6006, "common_not_found", "记录不存在"),
        COMMON_BANNER_EXIST(6007, "common_banner_exist", "该语言banner已存在"),
        COMMON_BANNER_TYPE_MUST_EXIST(6008, "common_banner_type_must_exist", "banner类型不能为空"),

        //====================  业务异常9000+  命名规范:business_xxx  ===================
        BUSINESS_PARAMETER_ERROR(9001, "business_parameter_error", "参数异常"),
        BUSINESS_ERROR(9002, "business_error", "业务异常"),
        BUSINESS_VERIFY_CODE_INVALID(9003, "business_verify_code_invalid", "验证码已失效"),
        BUSINESS_ERROR_VERIFY_CODE(9004, "business_error_verify_code", "验证码错误"),
        BUSINESS_COULD_NOT_DELETE(9005, "business_could_not_delete", "不能删除"),
        BUSINESS_SEND_EMAIL_ERROR(9006, "send_email_error", "发送邮件异常"),
        BUSINESS_TYPE_NOT_EXIST(9007, "business_type_not_exist", "类型不存在"),
        ONLY_USDT(9008,"only_usdt_price","暂时只支持usdt单位"),
        NONSUPPORT_EMAIL(9009,"NONSUPPORT_EMAIL","暂时不支持该邮箱"),
        TRANS_COIN_NOT_ALLOW_NULL(9010,"trans_coin_not_allow_null","转换币种不允许为空"),
        MONEY_TYPE_IS_NULL(9011,"money_type_is_null","币种表中找不到该币种"),

        //====================  活动异常10000+  命名规范:activity_xxx  ===================
        ACTIVITY_NOT_FOUND(10001, "activity_not_found", "活动不存在"),
        ACTIVITY_TIME_ERROR(10002, "time_error", "开始时间必须大于当前时间"),
        ACTIVITY_NOT_ALLOW_RETROACTIVE(10003, "not_allow_retroactive", "不允许补签"),
        ACTIVITY_HAS_RETROACTIVE(10004, "has_retroactive", "已经补签过"),
        ACTIVITY_NOT_IN_PART_TIME(10005, "not_in_part_time", "不在活动时间范围内"),
        ACTIVITY_TODAY_HAS_SIGN(10006, "today_has_sign", "今天已签到"),
        ACTIVITY_NO_RESIGN(10006, "no_resign", "无须补签或者活动不允许签到"),
        ACTIVITY_GIVE_COIN_MUST_GREATER_ZREO(10007, "give_coin_must_greater_zreo", "赠送币种数量必须大于 0 "),
        ACTIVITY_GIVE_PRODUCT_MUST_GREATER_ZREO(10008, "give_product_must_greater_zreo", "赠送产品数量必须大于 0 "),
        ACTIVITY_RETROACTIVE_COIN_MUST_GREATER_ZREO(10009, "retroactive_coin_must_greater_zreo", "补签成本必须大于 0 "),
        ACTIVITY_INVITE_RECORD_MUST_EXIST(10010, "invite_record_must_exist", "邀请奖励不能为空"),
        ACTIVITY_CURRENT_DAY_CAN_NOT_RETROACTIVE(10011, "current_day_can_not_retroactive", "当天不能进行补签,可点击签到"),
        ACTIVITY_RETROACTIVE_COST_MUST_EXIST(10012, "retroactive_vost_must_exist", "补签价格不能为空"),
        ACTIVITY_END_TIME_ERROR(10013, "end_time_error", "结束时间必须大于当前时间"),

        //====================  帮助中心异常11000+  命名规范:HELP_CENTER_xxx  ===================
        HELP_CENTER_THEME_MUST_EXIST(11001, "theme_must_exist", "主题不能为空"),
        HELP_CENTER_PROBLEM_MUST_EXIST(11002, "problem_must_exist", "问题不能为空"),
        HELP_CENTER_NOT_FOUND(11003, "center_not_found", "未找到对应的主题"),
        HELP_CENTER_PROBLEM_NOT_FOUND(11004, "problem_not_found", "未找到对应的问题"),
        EXPORT_EXCEL_ERROR(9010,"EXPORT_EXCEL_ERROR","导出excel失败"),


        BUSINESS_UPDATE_VERSION_INFO_NOT_EXIST(5100, "business_update_version_info_not_exist", "版本更新信息不存在"),
        BUSINESS_UPDATE_VERSION_INFO_ALREADY_EXISTS(51002, "business_update_version_info_already_exists", "版本更新信息已存在"),
        //====================  第三方异常6000+  命名规范:third_xxx  ===================
        THIRD_BANK_ERROR(6100,"THIRD_BANK_ERROR","数字银行错误"),
        WITHDRAW_ADDRESS_ERROR(6101,"WITHDRAW_ADDRESS_ERROR","提币地址错误，请重新填写");


        private int code;
        private String key;
        private String standardMessage;

        Status(int code, String key, String standardMessage) {
            this.code = code;
            this.standardMessage = standardMessage;
            this.key = key;
        }

        public int getCode() {
            return code;
        }

        public String getKey() {
            return key;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getStandardMessage() {
            return standardMessage;
        }

        public void setStandardMessage(String standardMessage) {
            this.standardMessage = standardMessage;
        }

        public static String getKeyByCode(Integer code) {
            for (Status value : Status.values()) {
                if (Objects.equals(code, value.getCode())) {
                    return value.getKey();
                }
            }
            return "";
        }
    }

}
