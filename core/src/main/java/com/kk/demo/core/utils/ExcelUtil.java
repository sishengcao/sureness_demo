package com.kk.demo.core.utils;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.handler.context.CellWriteHandlerContext;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.kk.demo.core.response.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author ksn
 * @version 1.0
 * @date 2021/11/17 14:59
 * @description: 生成excel工具类
 */
@Slf4j
public class ExcelUtil {

    private static final String TYPE = "java.math.BigDecimal";

    private static final int MAX_ROWS = 10000;

    public static void exportExcel(HttpServletResponse response, List<?> list, CellWriteHandler writeHandler, Class<?> clazz) {
        try {
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("utf-8");
            // 这里URLEncoder.encode可以防止中文乱码
            String fileName = URLEncoder.encode("data", "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
            ServletOutputStream outputStream = response.getOutputStream();
            ExcelWriterBuilder write = EasyExcel.write(outputStream, clazz);
            write.registerWriteHandler(new CellWriteHandler(){
                @Override
                public void afterCellDispose(CellWriteHandlerContext context) {
                    if (context != null) {
                        Cell cell = context.getCell();
                        if (!context.getHead() && context.getOriginalFieldClass() != null && TYPE.equals(context.getOriginalFieldClass().getName())) {
                            if (context.getFirstCellData() != null && context.getFirstCellData().getNumberValue() != null) {
                                BigDecimal bigDecimal = context.getFirstCellData().getNumberValue().setScale(8, BigDecimal.ROUND_HALF_UP);
                                if (cell != null) {
                                    cell.setCellValue(bigDecimal.toPlainString());
                                }
                            }
                        }
                    }
                }
            });
            if (writeHandler != null) {
                write = write.registerWriteHandler(writeHandler);
            }
            // write.autoCloseStream(Boolean.FALSE).sheet("sheet1").doWrite(list);
            // 动态生成多页sheet
            ExcelWriter excelWriter = write.build();
            // 分割的集合
            List<List<?>> lists = SplitList.splitList(list, MAX_ROWS);log.info(CollUtil.isNotEmpty(lists) ? JSONUtil.toJsonStr(lists.get(0)) : StrUtil.EMPTY);
            for (int i =1;i<=lists.size();i++){
                WriteSheet writeSheet = EasyExcel.writerSheet(i, "sheet" + i).build();
                excelWriter.write(lists.get(i-1), writeSheet);
            }
            excelWriter.finish();
        } catch (Exception e) {
            // 重置response
            response.reset();
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            ResponseUtil.toResponseEncryptWriter(response, ApiResponse.ofError(ApiResponse.Status.EXPORT_EXCEL_ERROR));
        }
    }

    static class SplitList{
        /**
         *
         * @author ksn
         * @Date 2021/11/24 16:50
         * @param list : 待切割集合
         * @param len : 集合按照多大size来切割
         * @return: java.util.List<java.util.List<T>>
         **/
        public static <T> List<List<?>> splitList(List<?> list, int len) {
            List<List<?>> result = new ArrayList<List<?>>();
            if (list == null || list.size() == 0 || len < 1) {
                return result;
            }
            int size = list.size();
            int count = (size + len - 1) / len;

            for (int i = 0; i < count; i++) {
                List<?> subList = list.subList(i * len, ((i + 1) * len > size ? size : len * (i + 1)));
                result.add(subList);
            }
            return result;
        }

        /**
         *
         * @author ksn
         * @Date 2021/11/24 16:49
         * @param source: 源集合
         * @param n:  分成n个集合
         * @return: java.util.List<java.util.List<T>>
         **/
        public static <T> List<List<T>> groupList(List<T> source, int n) {
            if (source == null || source.size() == 0 || n < 1) {
                return null;
            }
            if (source.size() < n) {
                return Arrays.asList(source);
            }
            List<List<T>> result = new ArrayList<List<T>>();
            int number = source.size() / n;
            int remaider = source.size() % n;
            int offset = 0; // 偏移量，每有一个余数分配，就要往右偏移一位
            for (int i = 0; i < n; i++) {
                List<T> list1 = null;
                if (remaider > 0) {
                    list1 = source.subList(i * number + offset , (i + 1) * number + offset + 1);
                    remaider--;
                    offset++;
                } else {
                    list1 = source.subList(i * number + offset, (i + 1) * number + offset);
                }
                result.add(list1);
            }

            return result;
        }
    }

}
