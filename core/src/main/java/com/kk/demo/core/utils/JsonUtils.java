package com.kk.demo.core.utils;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Map;

/**
 * @author: ziHeng
 * @description:
 * @create: 2020/10/21 20:35
 */
public class JsonUtils {

    // 定义jackson对象
    private static final ObjectMapper MAPPER = new ObjectMapper();

    /**
     * 将对象转换成json字符串。
     * <p>Title: pojoToJson</p>
     * <p>Description: </p>
     *
     * @param data
     * @return
     */
    public static String objectToJson(Object data) {
        return JSONObject.toJSONString(data).trim();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static String objectToJson2(Object data) {
        try {
            return MAPPER.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将json结果集转化为对象
     *
     * @param jsonData json数据
     * @param beanType 对象中的object类型
     * @return
     */
    public static <T> T jsonToPojoNotEncrypt(String jsonData, Class<T> beanType) {
        return JSONObject.parseObject(jsonData, beanType);
    }

    /**
     * 将json结果集转化为对象
     *
     * @param jsonData json数据
     * @param beanType 对象中的object类型
     * @return
     */
    public static <T> T jsonToPojo(String jsonData, Class<T> beanType) {
        return JSONObject.parseObject(jsonData, beanType);
    }

    /**
     * 将json数据转换成pojo对象list
     * <p>Title: jsonToList</p>
     * <p>Description: </p>
     *
     * @param jsonData
     * @param beanType
     * @return
     */
    public static <T> List<T> jsonToList(String jsonData, Class<T> beanType) {
        return JSONObject.parseArray(jsonData, beanType);
    }

    public static Map<String, Object> jsonToMap(String json) {
        try {
            return jsonToPojoNotEncrypt(json, Map.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
