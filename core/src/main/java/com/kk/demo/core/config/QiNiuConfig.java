package com.kk.demo.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/07/12 15:22
 */
@Component
@ConfigurationProperties(prefix = "qiniu")
@Data
public class QiNiuConfig {

    private String accessKeyId;


    private String secretKey;

    private String bucket;


}
