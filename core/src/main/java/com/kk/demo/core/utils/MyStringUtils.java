package com.kk.demo.core.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @date: 2021/6/10 15:38
 * @Author: AaronXu
 * @Description: 字符串工具类
 */
public class MyStringUtils {
    /**
     * 判断字符是否为空
     *
     * @param cs
     * @return
     */
    public static boolean isEmpty(CharSequence cs) {
        return cs == null || cs.length() == 0;
    }

    /*
     * 校验字符串是否含有中文
     */
    public static boolean isContainChinese(String... str) {
        for (int a = 0; a < str.length; a++) {
            Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
            Matcher m = p.matcher(str[a]);
            if (m.find())
                return true;
        }
        return false;
    }

    /*
     * 校验字符串是否为空
     */
    public static boolean stringIsNull(String... str) {
        for (int a = 0; a < str.length; a++) {
            if (isEmpty(str[a]))
                return true;
        }
        return false;
    }

    /**
     * 将map转为字符串
     *
     * @param {a:1,b:2,c:{d:3,c:4}}
     * @return a=1&b=2&d=3&c=4
     */
    public static String map2String(Map<String, Object> map) {
        StringBuilder returnStr = new StringBuilder();
        for (Map.Entry<String, Object> m : map.entrySet()) {
            if (m.getValue() == null || "".equals(m.getValue().toString()))
                continue;
            if (m.getValue() instanceof Map) {
                String str = map2String((Map<String, Object>) m.getValue());
                if (!"".equals(str)) {
                    if ("".equals(returnStr)) {
                        returnStr.append(str);
                    } else {
                        returnStr.append("&" + str);
                    }
                }
            } else {
                if ("".equals(returnStr.toString())) {
                    returnStr.append(m.getKey() + "=" + m.getValue());
                } else {
                    returnStr.append("&" + m.getKey() + "=" + m.getValue());
                }
            }
        }
        return returnStr.toString();
    }

    /**
     * 将用分隔符连接的字符串转为map
     *
     * @param str appId=xxx&expireTime=xxx&time=xxxx
     * @return {"appId":"xxx","expireTime":"xxx","time":"xxxx"}
     */
    public static Map<String, String> string2Map(String str, String separator) {
        Map<String, String> map = null;
        try {
            map = new HashMap<>();
            String[] strArr = str.split(separator);
            if (strArr.length < 1)
                return null;
            for (String s : strArr) {
                if (!isEmpty(s)) {
                    String[] arrArr = s.split("=");
                    if (arrArr.length != 2)
                        continue;
                    map.put(arrArr[0], arrArr[1]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return map;
        }
    }

    /**
     * 将用&分隔符连接的字符串转为map
     */
    public static Map<String, String> string2Map(String str) {
        return string2Map(str, "&");
    }

    /**
     * 排序
     *
     * @param paramsStr
     * @return a=1&b=2&c=3&d=4
     */
    public static String sortAndParseString(String paramsStr) {
        StringBuilder returnStr = new StringBuilder();
        String[] strArr = paramsStr.split("&");
        if (strArr.length == 1)
            return paramsStr;

        String[] keyArr = new String[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            keyArr[i] = strArr[i].split("=")[0];
        }
        keyArr = getUrlParam(keyArr);

        for (int i = 0; i < keyArr.length; i++) {
            for (int j = 0; j < strArr.length; j++) {
                if (keyArr[i].equals(strArr[j].split("=")[0])) {
                    if ("".equals(returnStr.toString())) {
                        returnStr.append(strArr[j]);
                    } else {
                        returnStr.append("&" + strArr[j]);
                    }
                }
            }
        }
        return returnStr.toString();
    }

    /**
     * @param
     * @return
     */
    public static String[] getUrlParam(String[] keys) {
        for (int i = 0; i < keys.length - 1; i++) {
            for (int j = 0; j < keys.length - i - 1; j++) {
                String pre = keys[j];
                String next = keys[j + 1];
                if (isMoreThan(pre, next)) {
                    String temp = pre;
                    keys[j] = next;
                    keys[j + 1] = temp;
                }
            }
        }
        return keys;
    }

    /**
     * 比较两个字符串的大小，按字母的ASCII码比较
     */
    private static boolean isMoreThan(String pre, String next) {
        if (null == pre || null == next || "".equals(pre) || "".equals(next)) {
            return false;
        }

        char[] c_pre = pre.toCharArray();
        char[] c_next = next.toCharArray();

        int minSize = Math.min(c_pre.length, c_next.length);

        for (int i = 0; i < minSize; i++) {
            if ((int) c_pre[i] > (int) c_next[i]) {
                return true;
            } else if ((int) c_pre[i] < (int) c_next[i]) {
                return false;
            }
        }
        if (c_pre.length > c_next.length) {
            return true;
        }
        return false;
    }

    /**
     * 隐藏部分身份证号码
     */
    /*private static String concealIdCard(String idCard) {
        if (!MyStringUtils.isEmpty(idCard)) {
            StringBuilder stringBuilder =
                    new StringBuilder(DESEncryptUtil.decrypt(idCard, IdCardKeyConfig.idCardStatic));
            stringBuilder.replace(6, 14, "********");
            return stringBuilder.toString();
        }
        return "";
    }*/
    public static String createOrderNumber(String prefix, int amount) {
        if (MyStringUtils.isEmpty(prefix)) {
            return RandomStringUtils.randomAlphabetic(amount);
        }
        return prefix + RandomStringUtils.randomAlphabetic(amount);
    }

    public static void main(String[] args) {
        System.out.println(createOrderNumber("po", 16));
    }

    /**
     * @return java.lang.String
     * @Description //生成验证码
     * @Date 19:06 2021/6/23
     * @Param [amount]
     **/
    public static String createVerifyCode(int amount) {
        return RandomStringUtils.randomNumeric(amount);
    }

    /**
     * @return java.lang.String
     * @Description //生成邀请码
     * @Date 19:06 2021/6/23
     * @Param [amount]
     **/
    public static String createInviteCode(int amount) {
        return RandomStringUtils.randomAlphabetic(amount);
    }


}
