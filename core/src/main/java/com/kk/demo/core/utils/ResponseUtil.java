package com.kk.demo.core.utils;



import com.kk.demo.core.response.ApiResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @description:
 * @author: ziHeng
 * @create: 2018-12-11 14:28
 **/
public class ResponseUtil {


    public static void toResponseEncryptWriter(HttpServletResponse response, ApiResponse result) {
        if (response == null) {
            return;
        }
        response.setContentType("application/json;charset=UTF-8");
        try {
            PrintWriter writer = response.getWriter();
            writer.write(JsonUtils.objectToJson(result));
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
