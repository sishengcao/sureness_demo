package com.kk.demo.core.enums;

import lombok.Getter;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/07/16 14:39
 */
@Getter
public enum HeaderEnums {

    PHONE_AREA("手机号地区","phoneArea"),
    DEVICE_TYPE("操作系统参数","deviceType"),
    SIGN_UP_SOURCE("注册来源 1:app 2:h5 3:web ,4:第三方,0:未知","signUpSource"),
    DEVICE_ID("设备id","deviceId"),
    LANGUAGE("语言","language"),
    VISIT_METHOD("访问方式","visitMethod")




    ;
    private String name;

    private String headerName;


    HeaderEnums(String name, String headerName) {
        this.name = name;
        this.headerName = headerName;
    }

}

