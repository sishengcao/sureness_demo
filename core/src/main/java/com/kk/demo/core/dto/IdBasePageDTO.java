package com.kk.demo.core.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/04/14 16:18
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class IdBasePageDTO extends BasePage{

    private Integer id;

}
