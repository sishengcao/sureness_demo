package com.kk.demo.core.enums.ProxyNodeAwardRecord;

import lombok.Getter;

/**
 * @author: ziHeng
 * @description: 印尼语
 * @create: 2021/08/29 14:22
 */
@Getter
public enum ProxyNodeAwardRecordTypeIdEnums {

    COMMISSION(1, "Proksi Biasa – Komisi"),
    CENT(2, "Proksi Biasa – Koin"),
    SUPER_NODE_COMISSION(3, "Proksi Super – Komisi"),
    SUPER_NODE_CENT(4, "Proksi Super – Koin"),


    COMMISSION_PEERS(10, "Proksi Biasa – Komisi (Level yang sama)"),
    CENT_PEERS(20, "Proksi Biasa – Koin (Level yang sama)"),
    SUPER_NODE_COMISSION_PEERS(30, "Proksi Super – Komisi (Level yang sama)"),
    SUPER_NODE_CENT_PEERS(40, "Proksi Super – Koin (Level yang sama)");

    private Integer type;

    private String name;

    ProxyNodeAwardRecordTypeIdEnums(Integer type, String name) {
        this.type = type;
        this.name = name;
    }
}
