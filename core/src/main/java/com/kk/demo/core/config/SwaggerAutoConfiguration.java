/*
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.kk.demo.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <p>
 * RESTful 服务 API 管理框架 Swagger 配置初始化
 * </p>
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerAutoConfiguration {

    @Bean
    public Docket managementApi() {
        // 添加head参数end
        return new Docket(DocumentationType.SWAGGER_2).groupName("管理后台接口")
                .apiInfo(apiInfo("管理后台接口", "1.0", "管理后台接口", "", "", "xx@qq.com")).select()
                // 为当前包路径
                .apis(RequestHandlerSelectors.basePackage("com.kk.demo.managementApi.controller")).paths(PathSelectors.any())
                .build();
    }

    /**
     * 接口信息
     * @return
     */
    private ApiInfo apiInfo(String title, String version, String desc, String name, String url, String email) {
        return new ApiInfoBuilder()
                // 页面标题
                .title(title)
                // 版本号
                .version(version)
                // 联系方式
                .contact(contact(name, url, email))
                // 描述
                .description(desc).build();
    }

    // 设置联系方式
    private Contact contact(String name, String url, String email) {
        return new Contact(name, url, email);
    }
}
