package com.kk.demo.core.enums.ProxyNodeAwardRecord;

import lombok.Getter;

/**
 * @author: ziHeng
 * @description: 阿拉伯语
 * @create: 2021/08/29 14:22
 */
@Getter
public enum ProxyNodeAwardRecordTypeArEnums {

    COMMISSION(1, "عمولة العقدة العادية"),
    CENT(2, "سنتات العقدة العادية"),
    SUPER_NODE_COMISSION(3, "عمولة العقدة الفائقة"),
    SUPER_NODE_CENT(4, "سنتات العقدة الفائقة"),


    COMMISSION_PEERS(10, "عمولة العقدة العادية (المستوى"),
    CENT_PEERS(20, "سنتات العقدة العادية (المستوى\"("),
    SUPER_NODE_COMISSION_PEERS(30, "عمولة العقدة الفائقة (المستوى\"("),
    SUPER_NODE_CENT_PEERS(40, "سنتات العقدة الفائقة (المستوى\"(");

    private Integer type;

    private String name;

    ProxyNodeAwardRecordTypeArEnums(Integer type, String name) {
        this.type = type;
        this.name = name;
    }
}
