package com.kk.demo.core.enums;

/**
 * 后台管理系统操作人类型枚举
 */
public enum SysOperateUserTypeEnum {
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    SYSTEM_USER,
}
