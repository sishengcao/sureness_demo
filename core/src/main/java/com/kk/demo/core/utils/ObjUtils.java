package com.kk.demo.core.utils;

import com.kk.demo.core.exceptions.BusinessException;
import org.apache.commons.beanutils.BeanMap;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author AaronXu
 * @date 2021/8/12 10:30
 */
public class ObjUtils {

    /**
     * 将对象转为Map
     *
     * @param obj
     * @return Map
     */
    public static Map<String, Object> convertObjectToMap(Object obj) {
        Map<String, Object> map = new HashMap<>();
        BeanMap beanMap = new BeanMap(obj);
        for (Object key : beanMap.keySet()) {
            if (key instanceof String && !"class".equals(key)) {
                map.put((String) key, beanMap.get(key));
            }
        }
        return map;
    }

    public static boolean isEmpty(Object obj) {
        return ObjectUtils.isEmpty(obj);
    }

    public static void isEmpty(Object obj, String message) {
        if (isEmpty(obj)) {
             throw new BusinessException(message);
        }
    }


}
