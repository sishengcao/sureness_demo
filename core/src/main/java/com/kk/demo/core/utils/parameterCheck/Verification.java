package com.kk.demo.core.utils.parameterCheck;

import com.kk.demo.core.exceptions.IllegalParameterException;
import com.kk.demo.core.response.ApiResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Field;

/**
 * @date: 2021/6/9 11:07
 * @Author: AaronXu
 * @Description: 更新或保存校验
 */
public class Verification {

    //开启保存或更新参数校验
    public static final String SAVE = "SAVE_OPEN";
    public static final String UPDATE = "UPDATE_OPEN";

    public static boolean verify(Object bean, String status) {
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            //获取实体上的注解
            Check check = field.getAnnotation(Check.class);
            if (check != null) {
                checkValue(check, field, bean, status);
            } else {
                //跳到下个字段
                continue;
            }
        }
        return true;
    }

    /**
     * 直接校验,不设置开关
     */
    public static boolean check(Object bean, String status, String... ignore) {
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);

            Check check = field.getAnnotation(Check.class);
            if (check != null) {
                //忽略校验字段
                boolean ig = false;
                String name = field.getName();
                if (ignore != null && ignore.length > 0) {
                    for (String i : ignore) {
                        if (i.equals(name)) {
                            ig = true;
                            break;
                        }
                    }
                }
                if (ig) {
                    continue;
                }
                checkValue(check, field, bean, status);
            } else {
                //跳到下个字段
                continue;
            }
        }
        return true;
    }

    private static void checkValue(Check check, Field field, Object bean, String status) {
        Object fieldValue = getFieldValue(field, bean);
        String fieldComment = "【" + check.comment() + "】 ";

        boolean contentBeNull = check.notNull();
        if (contentBeNull) {
            //新增校验全部字段
            if (StringUtils.isNotEmpty(status) && status.equals(SAVE)) {
                // 新增：校验全部字段
                checkNotNull(fieldComment, fieldValue);
                checkFormat(check, fieldComment, fieldValue);
            } else {
                //更新：校验非空字段（传参空不校验）
                if (!ObjectUtils.isEmpty(fieldValue)) {
                    checkFormat(check, fieldComment, fieldValue);
                }
            }
        } else {
            if (!ObjectUtils.isEmpty(fieldValue)) {
                checkFormat(check, fieldComment, fieldValue);
            }
        }
    }

    /**
     * 格式校验
     */
    private static boolean checkFormat(Check check, String comment, Object fieldVal) {
        // 最大长度校验
        if (check.maxLen() > 8) {
            if (BaseTypeUtil.getLength(fieldVal) > check.maxLen()) {
                throw new IllegalParameterException(ApiResponse.Status.CUSTOM_LENGTH_TOO_LONG);
            }
        }
        // 最小长度校验
        if (check.minLen() > 1) {
            if (BaseTypeUtil.getLength(fieldVal) < check.minLen()) {
                throw new IllegalParameterException(ApiResponse.Status.CUSTOM_LENGTH_TOO_SHORT);
            }
        }
        // 整数校验
        if (check.numeric()) {
            if (!PatternUtil.isNumeric(String.valueOf(fieldVal))) {
                throw new IllegalParameterException(ApiResponse.Status.CUSTOM_ONLY_ALLOW_INTEGERS);
            }
        }
        // 小数校验
        if (check.decimal()) {
            if (!PatternUtil.isDecimal(String.valueOf(fieldVal))) {
                throw new IllegalParameterException(ApiResponse.Status.CUSTOM_ERROR_PRECISION);
            }
        }

        // 7-固定电话校验
        if (check.phone()) {
            if (!PatternUtil.isPhone(String.valueOf(fieldVal))) {
                throw new IllegalParameterException(ApiResponse.Status.CUSTOM_ERROR_LANDLINE_NUMBER);
            }
        }
        // 8-手机号码校验
        if (check.mobile()) {

            if (!PatternUtil.isMobile(String.valueOf(fieldVal))) {
                throw new IllegalParameterException(ApiResponse.Status.CUSTOM_ERROR_PHONE_NUMBER);
            }
        }

        // 邮箱校验
        if (check.mail()) {
            if (!PatternUtil.isEmail(String.valueOf(fieldVal))) {
                throw new IllegalParameterException(ApiResponse.Status.CUSTOM_ERROR_EMAIL);
            }
        }
        // IP地址校验
        if (check.ip()) {
            if (!PatternUtil.isIP(String.valueOf(fieldVal))) {
                throw new IllegalParameterException(ApiResponse.Status.CUSTOM_INVALID_IP);
            }
        }
        // URL校验
        if (check.URL()) {
            if (!PatternUtil.isURL(String.valueOf(fieldVal))) {
                throw new IllegalParameterException(ApiResponse.Status.CUSTOM_INVALID_URL);
            }
        }
        // 密码校验
        if (check.password()) {
            if (!PatternUtil.isPassword(String.valueOf(fieldVal))) {
                throw new IllegalParameterException(ApiResponse.Status.CUSTOM_ERROR_PASSWORD_FORMAT);
            }
        }
        return true;
    }

    /**
     * 非空校验,拼接字段描述
     */
    private static boolean checkNotNull(String comment, Object fieldVal) {
        if (fieldVal == null || "".equals(fieldVal.toString())) {
            throw new IllegalParameterException(ApiResponse.Status.CUSTOM_COULD_NOT_BE_EMPTY, comment);
        }
        return true;
    }

    /**
     * get field value
     */
    private static Object getFieldValue(Field field, Object bean) {
        try {
            return field.get(bean);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 判断域对象是否为空对象或字符串。<br>
     * 如果是int long类型，默认值为0；<br>
     * 如果是float double类型，默认值为0.0；<br>
     */
    private static boolean valueIsNotNull(Object value) {
        if (value != null && !"".equals(value.toString())) {
            return true;
        }
        return false;
    }
}
