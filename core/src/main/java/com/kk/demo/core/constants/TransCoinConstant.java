package com.kk.demo.core.constants;

import lombok.Getter;

/**
 * @author sishengcao
 * @date 2021/11/8 10:58
 **/
@Getter
public class TransCoinConstant {

    public static final String COIN_RATE = "coin.rate.";
    public static final long DEFAULT_EXPIRE = 60 * 5; // 默认的缓存时间 默认60分钟 * 5 小时 , 设置缓存单位 TimeUnit.MINUTES

}
