package com.kk.demo.core.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import com.kk.demo.core.exceptions.BusinessException;
import lombok.Getter;

/**
 * 多语言表类型枚举
 */
@Getter
public enum MultiLanguageTypeEnums {
    // ------------产品相关多语言------------
    /*产品名称*/
    PRODUCT_NAME("PRODUCT_NAME", "产品名称"),
    /*产品类型*/
    PRODUCT_TYPE("PRODUCT_TYPE", "产品类型"),
    /*矿池名称*/
    POOL_NAME("POOL_NAME", "矿池名称"),
    /*产品sku名称*/
    PRODUCT_SKU_NAME("PRODUCT_SKU_NAME", "产品sku名称"),
    /*产品标签*/
    PRODUCT_LABEL("PRODUCT_LABEL", "产品标签"),
    /*产品描述*/
    PRODUCT_DESC("PRODUCT_DESC", "产品描述", true),
    /*产品详情*/
    PRODUCT_DETAILS("PRODUCT_DETAILS", "产品详情", true),
    /*产品风险说明*/
    PRODUCT_RISK_DESCRIPTION("PRODUCT_RISK_DESCRIPTION", "产品风险说明", true),
    /* 代理节点说明*/
    NODE_CONTENT("NODE_CONTENT","代理节点说明",true),
    /* 产品返回数量说明*/
    BACK_COUNTS("BACK_COUNTS","产品返回数量说明",true),
    /* 空投文本说明*/
    BONUS_CONTENT("BONUS_CONTENT","空投文本",true),
    /**
     * 优惠券名称
     */
    COUPON_NAME("COUPON_NAME","优惠券名称"),

    /**
     * 优惠券详情
     */
    COUPON_DES("COUPON_DES","优惠券详情"),
    // -----------系统消息---------
    /*消息标题*/
    SYS_MSG_TITLE("SYS_MSG_TITLE", "消息标题"),
    /*消息正文*/
    SYS_MSH_DESC("SYS_MSH_DESC", "消息正文"),
    // -----------------币种钱包相关多语言--------
    /*钱包转入注意事项*/
    WALLET_TRANS_IN_NOTICE("WALLET_TRANS_IN_NOTICE", "钱包转入注意事项", true),
    /*钱包转出注意事项*/
    WALLET_TRANS_OUT_NOTICE("WALLET_TRANS_OUT_NOTICE", "钱包转出注意事项", true),
    //--------------------公告、资讯、图片-----------------------
    ANNOUNCEMENT_TITLE("ANNOUNCEMENT_TITLE", "公告标题"),
    NEW_TITLE("NEW_TITLE", "资讯标题"),
    IMAGE_TITLE("IMAGE_TITLE", "图片标题"),
    HELP_CONTER_TITLE("HELP_CONTER_TITLE","帮助中心"),
    HELP_CONTER_CONTEN("HELP_CONTER_CONTEN","帮助中心回答富文本",true),
    HELP_CENTER_PROBLEM("HELP_CENTER_PROBLEM","问题标识"),
    //------------------其他---------------------problem
    /*服务协议*/
    SERVICE_AGREEMENT("SERVICE_AGREEMENT", "服务协议", true),
    DOCUMENT_CONTENT("DOCUMENT_CONTENT", "公告", true),
    NEWS("NEWS","新闻资讯内容",true),
    /*用户协议*/
    USER_AGREEMENT("USER_AGREEMENT","用户协议",true),
    /**
     * 签到活动名称
     */
    SIGN_IN_ACTIVITY_NAME("SIGN_IN_ACTIVITY_NAME","签到活动名称"),
    /**
     * 邮箱邀请活动名称
     */
    EMAIL_INVITE_ACTIVITY_NAME("EMAIL_INVITE_ACTIVITY_NAME","邮箱邀请活动名称"),

    /*banner*/
    WEB_SLIDE_BANNER("WEB_SLIDE_BANNER","官网banner"),
    /*h5_banner*/
    H5_SLIDE_BANNER("H5_SLIDE_BANNER","H5的banner"),

    /*帮助中心*/
    HELP_CENTER_THEME("HELP_CENTER_THEME","帮助中心主题"),
    HELP_CENTER_QUESTION("HELP_CENTER_QUESTION","帮助中心问题"),
    HELP_CENTER_ANSWER("HELP_CENTER_ANSWER","帮助中心回答")
    ;

    @EnumValue

    private final String value;

    private final String describe;

    private boolean richTextTag = false;

    MultiLanguageTypeEnums(String value, String describe) {
        this.value = value;
        this.describe = describe;
    }

    MultiLanguageTypeEnums(String value, String describe, boolean richTextTag) {
        this.value = value;
        this.describe = describe;
        this.richTextTag = richTextTag;
    }

    @JsonValue
    public String getValue() {
        return this.value;
    }

    public static MultiLanguageTypeEnums fromName(String name) {
        if (null == name) {
            return null;
        }
        for (MultiLanguageTypeEnums b : MultiLanguageTypeEnums.values()) {
            if (b.value.equalsIgnoreCase(name)) {
                return b;
            }
        }
        throw new BusinessException("不存在这种多语言类型:" + name);
    }
}
