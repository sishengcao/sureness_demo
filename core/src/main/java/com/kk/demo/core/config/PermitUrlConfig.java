package com.kk.demo.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author: ziHeng
 * @description:
 * @create: 2020/12/08 16:30
 */
@Component
@ConfigurationProperties(prefix = "permit")
@Data
public class PermitUrlConfig {

    private String[] urls;


}
