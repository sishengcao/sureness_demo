package com.kk.demo.core.utils;


import java.util.Random;

public class RandomNumUtil {
    private static char[] codeSequence = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N','P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
            'X', 'Y', 'Z', 'a','b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n','p', 'q', 'r', 's', 't', 'u', 'v', 'w',
            'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    public static String getRandomNumPinYin(int codeCount) {
        StringBuffer randomCode = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < codeCount; i++) {
            String strRand = String.valueOf(codeSequence[random.nextInt(codeSequence.length)]);
            randomCode.append(strRand);
        }
        return randomCode.toString();
    }

    public static Integer getFanWeiNum(Integer min,Integer max){
        return min + (int)(Math.random() * ((max - min) + 1));
    }

    public static void main(String[] args){
        for (int i = 0; i < 10; i++) {
            System.out.println(getFanWeiNum(1,50));
        }
    }

}

