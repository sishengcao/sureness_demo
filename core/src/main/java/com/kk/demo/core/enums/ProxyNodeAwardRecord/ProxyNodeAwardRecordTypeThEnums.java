package com.kk.demo.core.enums.ProxyNodeAwardRecord;

import lombok.Getter;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/08/29 14:22
 */
@Getter
public enum ProxyNodeAwardRecordTypeThEnums {

    COMMISSION(1, "จำนวนรายได้สะสม"),
    CENT(2, "โหนดทั่วไป"),
    SUPER_NODE_COMISSION(3, "คณะกรรมการซุปเปอร์โหนด"),
    SUPER_NODE_CENT(4, "ซุปเปอร์โหนด-เซ็นต์\"(Super Node-Cents)"),


    COMMISSION_PEERS(10, "ค่าคอมมิชชันโหนดสามัญ (ระดับ)"),
    CENT_PEERS(20, "โหนดเซ็นต์สามัญ (ระดับ)"),
    SUPER_NODE_COMISSION_PEERS(30, "คณะกรรมการซุปเปอร์โหนด (ระดับ)"),
    SUPER_NODE_CENT_PEERS(40, "ซุปเปอร์โหนด-เซ็นต์\"(Super Node-Cents)(ระดับ)");

    private Integer type;

    private String name;

    ProxyNodeAwardRecordTypeThEnums(Integer type, String name) {
        this.type = type;
        this.name = name;
    }
}
