package com.kk.demo.core.dto;

import lombok.Data;

import java.util.List;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/08/27 10:00
 */
@Data
public class IdListDTO {

    //idList
    private List<Integer> idList;

}
