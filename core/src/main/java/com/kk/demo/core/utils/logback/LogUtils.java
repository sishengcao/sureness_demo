package com.kk.demo.core.utils.logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * log工具类
 *
 * @author Joe
 * @date 2021/06/21 21:34
 **/
public class LogUtils {

    public static final Logger systemLog = LoggerFactory.getLogger("SystemLogger");

    public static final Logger accessLog = LoggerFactory.getLogger("AccessLogger");

    public static final Logger statLog = LoggerFactory.getLogger("StatLogger");

    public static final Logger infoLog = LoggerFactory.getLogger("InfoLogger");

    public static final Logger jobLog = LoggerFactory.getLogger("JobLogger");

    public static final Logger aliLog = LoggerFactory.getLogger("AliLogger");

}
