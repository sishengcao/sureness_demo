package com.kk.demo.core.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collections;
import java.util.List;

/**
 * @author AaronXu
 * @date 2020/11/3 16:36
 */
public class MyCollectionUtils {

    //list随机获取一条  list自行做好空判断
    public static <T> T randomGetOne(List<?> list, Class<T> classType) {
        Collections.shuffle(list);
        return new ObjectMapper().convertValue(list.get(0), classType);
    }
}
