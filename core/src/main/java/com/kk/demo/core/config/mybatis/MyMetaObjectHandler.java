package com.kk.demo.core.config.mybatis;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.kk.demo.core.enums.StatusEnum;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Objects;


@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {

        this.setFieldValByName("deleted", 0, metaObject);

        if (Objects.isNull(getFieldValByName("addTime", metaObject)))
            this.setFieldValByName("addTime", new Date(), metaObject);
        if (Objects.isNull(getFieldValByName("updateTime", metaObject)))
            this.setFieldValByName("updateTime", new Date(), metaObject);
        if (Objects.isNull(getFieldValByName("createTime", metaObject)))
            this.setFieldValByName("createTime", new Date(), metaObject);
        if (Objects.isNull(getFieldValByName("status", metaObject)))
            this.setFieldValByName("status", StatusEnum.Y.getStatus(), metaObject);
        if (Objects.isNull(getFieldValByName("state", metaObject)))
            this.setFieldValByName("state", StatusEnum.Y.getStatus(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        if (Objects.isNull(getFieldValByName("updateTime", metaObject)))
        this.setFieldValByName("updateTime", new Date(), metaObject);
    }
}
