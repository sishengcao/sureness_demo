package com.kk.demo.core.enums.ProxyNodeAwardRecord;

import lombok.Getter;

/**
 * @author: ziHeng
 * @description:
 * @create: 2021/08/29 14:22
 */
@Getter
public enum ProxyNodeAwardRecordTypeEnums {

    COMMISSION(1, "普通节点-提成"),
    CENT(2, "普通节点-分币"),
    SUPER_NODE_COMISSION(3, "超级节点-提成"),
    SUPER_NODE_CENT(4, "超级节点-分币"),


    COMMISSION_PEERS(10, "普通节点-提成(平级)"),
    CENT_PEERS(20, "普通节点-分币(平级)"),
    SUPER_NODE_COMISSION_PEERS(30, "超级节点-提成(平级)"),
    SUPER_NODE_CENT_PEERS(40, "超级节点-分币(平级)");

    private Integer type;

    private String name;

    ProxyNodeAwardRecordTypeEnums(Integer type, String name) {
        this.type = type;
        this.name = name;
    }
}
