package com.kk.demo.user.service.impl;/**
 * @author sishengcao
 * @date 2021/12/17 14:33
 **/

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kk.demo.user.entity.UserRole;
import com.kk.demo.user.mapper.UserRoleMapper;
import com.kk.demo.user.service.IUserRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author sishengcao
 * @date 2021年12月17日 14:33
 * @describe : 
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteRoleResourceBind(Long roleId, Long userId) {
        baseMapper.deleteRoleResourceBind(roleId,userId);
    }
}
