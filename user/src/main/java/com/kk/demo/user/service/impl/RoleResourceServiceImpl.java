package com.kk.demo.user.service.impl;/**
 * @author sishengcao
 * @date 2021/12/17 17:41
 **/

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kk.demo.user.entity.RoleResource;
import com.kk.demo.user.mapper.RoleResourceMapper;
import com.kk.demo.user.service.IRoleResourceService;
import org.springframework.stereotype.Service;

/**
 * @author sishengcao
 * @date 2021年12月17日 17:41
 * @describe : 
 */
@Service
public class RoleResourceServiceImpl extends ServiceImpl<RoleResourceMapper, RoleResource> implements
    IRoleResourceService {

    @Override public void deleteRoleResourceBind(Long roleId, Long resourceId) {
        baseMapper.deleteRoleResourceBind(roleId,resourceId);
    }
}
