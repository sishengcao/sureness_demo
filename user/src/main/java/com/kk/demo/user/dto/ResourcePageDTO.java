package com.kk.demo.user.dto;/**
 * @author sishengcao
 * @date 2021/12/17 16:46
 **/

import com.kk.demo.core.dto.BasePage;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author sishengcao
 * @date 2021年12月17日 16:46
 * @describe : 
 */
@Data
@ApiModel("资源分页对象")
public class ResourcePageDTO extends BasePage {
}
