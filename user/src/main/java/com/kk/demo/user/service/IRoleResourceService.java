package com.kk.demo.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kk.demo.user.entity.RoleResource;

/**
 * @author sishengcao
 * @date 2021/12/17 17:41
 **/
public interface IRoleResourceService extends IService<RoleResource> {
    void deleteRoleResourceBind(Long roleId, Long resourceId);
}
