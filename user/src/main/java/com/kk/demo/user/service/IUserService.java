package com.kk.demo.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kk.demo.user.entity.User;

import java.util.List;
import java.util.Optional;

/**
 * @author sishengcao
 * @date 2021/12/17 14:23
 **/
public interface IUserService extends IService<User> {
    Optional<User> findUserByUsername(String username);

    List<String> findAccountOwnRoles(String username);
}
