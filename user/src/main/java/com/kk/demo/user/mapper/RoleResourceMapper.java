package com.kk.demo.user.mapper;/**
 * @author sishengcao
 * @date 2021/12/17 17:41
 **/

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kk.demo.user.entity.RoleResource;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;

/**
 * @author sishengcao
 * @date 2021年12月17日 17:41
 * @describe : 
 */
@Mapper
public interface RoleResourceMapper extends BaseMapper<RoleResource> {

    @Delete("delete from role_resource bind where bind.role_id = #{roleId} and bind.resource_id = #{resourceId}")
    void deleteRoleResourceBind(@Param("roleId") Long roleId, @Param("resourceId") Long resourceId);
}
