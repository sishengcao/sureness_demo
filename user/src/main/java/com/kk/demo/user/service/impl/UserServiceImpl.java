package com.kk.demo.user.service.impl;/**
 * @author sishengcao
 * @date 2021/12/17 14:23
 **/

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kk.demo.user.entity.User;
import com.kk.demo.user.mapper.UserMapper;
import com.kk.demo.user.service.IUserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author sishengcao
 * @date 2021年12月17日 14:23
 * @describe : 
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Override
    public Optional<User> findUserByUsername(String username) {
        QueryWrapper<User> query = new QueryWrapper<>();
        query.lambda().eq(User::getUsername,username);
        User user = baseMapper.selectOne(query);
        if(Objects.isNull(user)){
            return Optional.ofNullable(user);
        }
        return Optional.of(user);
    }

    @Override
    public List<String> findAccountOwnRoles(String username) {
        return baseMapper.findAccountOwnRoles(username);
    }
}
