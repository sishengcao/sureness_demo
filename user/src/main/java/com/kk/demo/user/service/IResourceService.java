package com.kk.demo.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kk.demo.user.entity.Resource;

import java.util.Set;

/**
 * @author sishengcao
 * @date 2021/12/17 14:20
 **/
public interface IResourceService extends IService<Resource> {
    boolean existsById(Long resourceId);

    Set<String> getAllEnableResourcePath();

    Set<String> getAllOpenResourcePath();
}
