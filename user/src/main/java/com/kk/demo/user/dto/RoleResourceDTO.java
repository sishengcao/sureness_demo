package com.kk.demo.user.dto;/**
 * @author sishengcao
 * @date 2021/12/17 17:16
 **/

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author sishengcao
 * @date 2021年12月17日 17:16
 * @describe : 
 */
@Data
@ApiModel("角色资源请求")
public class RoleResourceDTO {

    private Long roleId;

    private Long resourceId;
}
