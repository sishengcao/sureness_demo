package com.kk.demo.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kk.demo.user.dto.RoleResourcePageDTO;
import com.kk.demo.user.entity.Resource;
import com.kk.demo.user.entity.Role;

/**
 * @author sishengcao
 * @date 2021/12/17 14:22
 **/
public interface IRoleService extends IService<Role> {
    boolean existsById(Long roleId);

    IPage<Resource> getPageResourceOwnRole(RoleResourcePageDTO dto);

    IPage<Resource> getPageResourceNotOwnRole(RoleResourcePageDTO dto);
}
