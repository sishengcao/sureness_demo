package com.kk.demo.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * resource entity
 * @author tomsun28
 * @date 00:00 2019-07-26
 */
@Data
@TableName("resource")
@ApiModel("资源实体")
public class Resource {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private String code;

    private String uri;

    private String type;

    private String method;

    private Integer status;

    private String description;

    private LocalDateTime gmtCreate;

    private LocalDateTime gmtUpdate;
}
