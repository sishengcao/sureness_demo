package com.kk.demo.user.service.impl;/**
 * @author sishengcao
 * @date 2021/12/17 14:21
 **/

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kk.demo.core.utils.CollectionUtils;
import com.kk.demo.user.entity.Resource;
import com.kk.demo.user.mapper.ResourceMapper;
import com.kk.demo.user.service.IResourceService;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author sishengcao
 * @date 2021年12月17日 14:21
 * @describe : 
 */
@Service
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper, Resource> implements IResourceService {

    @Override public boolean existsById(Long resourceId) {
        Resource resource = baseMapper.selectById(resourceId);
        if(Objects.isNull(resource)){
            return false;
        }
        return true;
    }

    @Override
    public Set<String> getAllEnableResourcePath() {
        List<String> pathList = baseMapper.getAllEnableResourcePath();
        Optional<List<String>> optional;
        if(CollectionUtils.isEmpty(pathList)){
            optional = Optional.ofNullable(pathList);
        }else {
            optional = Optional.of(pathList);
        }
        return optional.<Set<String>>map(HashSet::new).orElseGet(() -> new HashSet<>(0));
    }

    @Override
    public Set<String> getAllOpenResourcePath() {
        List<String> pathList = baseMapper.getAllOpenResourcePath();
        Optional<List<String>> optional;
        if(CollectionUtils.isEmpty(pathList)){
            optional = Optional.ofNullable(pathList);
        }else {
            optional = Optional.of(pathList);
        }
        return optional.<Set<String>>map(HashSet::new).orElseGet(() -> new HashSet<>(0));
    }
}
