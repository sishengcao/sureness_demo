package com.kk.demo.user.dto;/**
 * @author sishengcao
 * @date 2021/12/17 17:22
 **/

import com.kk.demo.core.dto.BasePage;
import lombok.Data;

/**
 * @author sishengcao
 * @date 2021年12月17日 17:22
 * @describe : 
 */
@Data
public class RolePageDTO extends BasePage {
}
