package com.kk.demo.user.dto;/**
 * @author sishengcao
 * @date 2021/12/17 17:11
 **/

import com.kk.demo.core.dto.BasePage;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author sishengcao
 * @date 2021年12月17日 17:11
 * @describe : 
 */
@Data
@ApiModel("资源角色分页对象")
public class RoleResourcePageDTO extends BasePage {

    private Long roleId;
}
