package com.kk.demo.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kk.demo.user.entity.UserRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;

/**
 * @author sishengcao
 * @date 2021/12/17 14:33
 **/
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {

    @Delete("delete from user_role bind where bind.role_id = #{roleId} and bind.user_id = #{userId}")
    void deleteRoleResourceBind(@Param("roleId") Long roleId, @Param("userId") Long userId);
}
