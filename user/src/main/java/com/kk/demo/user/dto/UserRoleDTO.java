package com.kk.demo.user.dto;/**
 * @author sishengcao
 * @date 2021/12/17 17:58
 **/

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author sishengcao
 * @date 2021年12月17日 17:58
 * @describe : 
 */
@Data
@ApiModel("用户权限")
public class UserRoleDTO {

    private String username;

    private Long roleId;
}
