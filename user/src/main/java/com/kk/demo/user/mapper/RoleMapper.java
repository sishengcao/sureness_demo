package com.kk.demo.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kk.demo.user.entity.Resource;
import com.kk.demo.user.entity.Role;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

/**
 * @author sishengcao
 * @date 2021/12/17 14:18
 **/
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

    @Select("select distinct resource.* from resource resource \n"
        + "left join role_resource bind on bind.resource_id = resource.id \n" + "where bind.role_id = #{roleId}\n"
        + "order by resource.id asc")
    IPage<Resource> getPageResourceOwnRole(@Param("page") Page page, @Param("roleId") Long roleId);

    @Select("select distinct resource.* from resource resource \n" + " where resource.id not in \n"
        + "(select distinct bind.resource_id from role_resource bind where bind.role_id = #{roleId}) \n"
        + "order by resource.id asc ")
    IPage<Resource> getPageResourceNotOwnRole(@Param("page") Page page, @Param("roleId") Long roleId);
}
