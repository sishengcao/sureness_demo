package com.kk.demo.user.service.impl;/**
 * @author sishengcao
 * @date 2021/12/17 14:22
 **/

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kk.demo.user.dto.RoleResourcePageDTO;
import com.kk.demo.user.entity.Resource;
import com.kk.demo.user.entity.Role;
import com.kk.demo.user.mapper.RoleMapper;
import com.kk.demo.user.service.IRoleService;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author sishengcao
 * @date 2021年12月17日 14:22
 * @describe : 
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

    @Override public boolean existsById(Long roleId) {
        Role role = baseMapper.selectById(roleId);
        if(Objects.isNull(role)){
            return false;
        }
        return true;
    }

    @Override public IPage<Resource> getPageResourceOwnRole(RoleResourcePageDTO dto) {
        return baseMapper.getPageResourceOwnRole(new Page<>(dto.getPageNum(),dto.getPageSize()),dto.getRoleId());
    }

    @Override public IPage<Resource> getPageResourceNotOwnRole(RoleResourcePageDTO dto) {
        return baseMapper.getPageResourceNotOwnRole(new Page<>(dto.getPageNum(),dto.getPageSize()),dto.getRoleId());
    }
}
