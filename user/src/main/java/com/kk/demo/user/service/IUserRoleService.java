package com.kk.demo.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kk.demo.user.entity.UserRole;

/**
 * @author sishengcao
 * @date 2021/12/17 14:33
 **/
public interface IUserRoleService extends IService<UserRole> {
    void deleteRoleResourceBind(Long roleId, Long userId);
}
