package com.kk.demo.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kk.demo.user.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author sishengcao
 * @date 2021/12/17 14:18
 **/
@Mapper
public interface UserMapper extends BaseMapper<User> {

    @Select("select ar.code from role ar, user au, user_role bind \n"
        + "where ar.id = bind.role_id and au.id = bind.user_id and au.username = #{username}")
    List<String> findAccountOwnRoles(@Param("username") String username);
}
