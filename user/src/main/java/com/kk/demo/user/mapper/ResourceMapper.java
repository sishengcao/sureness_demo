package com.kk.demo.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kk.demo.user.entity.Resource;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author sishengcao
 * @date 2021/12/17 14:20
 **/
@Mapper
public interface ResourceMapper extends BaseMapper<Resource> {

    @Select("SELECT  CONCAT(LOWER(res.uri),'===',LOWER(res.method),'===[',IFNULL(GROUP_CONCAT(DISTINCT role.code),''),']')\n"
        + "FROM resource res \n" + "LEFT JOIN role_resource bind on res.id = bind.resource_id \n"
        + "LEFT JOIN role role on role.id = bind.role_id \n" + "where res.status = 1 \n" + "group by res.id")
    List<String> getAllEnableResourcePath();

    @Select("select CONCAT(LOWER(resource.uri),'===', resource.method) \n"
        + "from resource resource where resource.status = 9 order by resource.id")
    List<String> getAllOpenResourcePath();
}
