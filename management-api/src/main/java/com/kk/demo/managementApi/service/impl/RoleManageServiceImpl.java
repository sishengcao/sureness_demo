package com.kk.demo.managementApi.service.impl;/**
 * @author sishengcao
 * @date 2021/12/17 16:36
 **/

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kk.demo.managementApi.service.IRoleManageService;
import com.kk.demo.user.dto.RolePageDTO;
import com.kk.demo.user.dto.RoleResourceDTO;
import com.kk.demo.user.dto.RoleResourcePageDTO;
import com.kk.demo.user.entity.Resource;
import com.kk.demo.user.entity.Role;
import com.kk.demo.user.entity.RoleResource;
import com.kk.demo.user.service.IResourceService;
import com.kk.demo.user.service.IRoleResourceService;
import com.kk.demo.user.service.IRoleService;
import com.usthe.sureness.matcher.TreePathRoleMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author sishengcao
 * @date 2021年12月17日 16:36
 * @describe : 
 */
@Service
public class RoleManageServiceImpl implements IRoleManageService {

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IResourceService resourceService;

    @Autowired
    private IRoleResourceService roleResourceService;

    @Autowired
    private TreePathRoleMatcher treePathRoleMatcher;

    @Override public IPage<Resource> getPageResourceOwnRole(RoleResourcePageDTO dto) {
        return roleService.getPageResourceOwnRole(dto);
    }

    @Override public IPage<Resource> getPageResourceNotOwnRole(RoleResourcePageDTO dto) {
        return roleService.getPageResourceNotOwnRole(dto);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void authorityRoleResource(RoleResourceDTO dto) {
        // Determine whether this resource and role exist
        if (!roleService.existsById(dto.getRoleId()) || !resourceService.existsById(dto.getResourceId())) {
            throw new RuntimeException("roleId or resourceId not exist");
        }
        // insert it in database, if existed the unique index will work
        RoleResource bind = RoleResource
            .builder().roleId(dto.getRoleId()).resourceId(dto.getResourceId()).build();
        roleResourceService.save(bind);
        // refresh resource path data tree
        treePathRoleMatcher.rebuildTree();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteAuthorityRoleResource(RoleResourceDTO dto) {
        roleResourceService.deleteRoleResourceBind(dto.getRoleId(), dto.getResourceId());
        // refresh resource path data tree
        treePathRoleMatcher.rebuildTree();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean addRole(Role role) {
        return roleService.save(role);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateRole(Role role) {
        return roleService.updateById(role);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteRole(Long roleId) {
        return roleService.removeById(roleId);
    }

    @Override public IPage<Role> getPageRole(RolePageDTO dto) {
        QueryWrapper<Role> query = new QueryWrapper<>();
        query.lambda().orderByDesc(Role::getGmtCreate);
        return roleService.page(new Page<>(dto.getPageNum(),dto.getPageSize()),query);
    }
}
