package com.kk.demo.managementApi.service.impl;/**
 * @author sishengcao
 * @date 2021/12/17 16:37
 **/

import com.kk.demo.managementApi.service.IUserRoleManageService;
import com.kk.demo.user.dto.UserRoleDTO;
import com.kk.demo.user.entity.User;
import com.kk.demo.user.entity.UserRole;
import com.kk.demo.user.service.IUserRoleService;
import com.kk.demo.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author sishengcao
 * @date 2021年12月17日 16:37
 * @describe : 
 */
@Service
public class UserRoleManageServiceImpl implements IUserRoleManageService {

    @Autowired
    private IUserService userService;

    @Autowired
    private IUserRoleService userRoleService;

    @Override public List<String> loadAccountRoles(String appId) {
        return userService.findAccountOwnRoles(appId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean authorityUserRole(UserRoleDTO dto) {
        Optional<User> optional = userService.findUserByUsername(dto.getUsername());
        if (!optional.isPresent()) {
            return false;
        }
        Long userId = optional.get().getId();
        UserRole userRoleBindDO = UserRole.builder().userId(userId).roleId(dto.getRoleId()).build();

        return userRoleService.save(userRoleBindDO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteAuthorityUserRole(UserRoleDTO dto) {
        Optional<User> optional = userService.findUserByUsername(dto.getUsername());
        if (!optional.isPresent()) {
            return false;
        }
        Long userId = optional.get().getId();
        userRoleService.deleteRoleResourceBind(dto.getRoleId(), userId);
        return true;
    }
}
