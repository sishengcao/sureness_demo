package com.kk.demo.managementApi.controller;

import com.kk.demo.core.response.ApiResponse;
import com.kk.demo.managementApi.service.AccountService;
import com.kk.demo.sureness.tokenStore.RedisTokenStore;
import com.kk.demo.user.dto.Account;
import com.usthe.sureness.provider.annotation.WithoutAuth;
import com.usthe.sureness.util.JsonWebTokenUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author tomsun28
 * @date 00:24 2019-08-01
 */
@Api(tags = "账号认证相关接口")
@RestController
@Slf4j
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private RedisTokenStore redisTokenStore;

    @WithoutAuth(mapping = "/auth/token",method = "issueJwtToken")
    @ApiOperation(value = "获取token",notes="获取token")
    @PostMapping("/auth/token")
//    @SysLog(operType = SysOperateTypeEnum.GRANT,userType = SysOperateUserTypeEnum.SYSTEM_USER)
    public ApiResponse issueJwtToken(@RequestBody @Validated Account account) {
        boolean authenticatedFlag = accountService.authenticateAccount(account);
        if (!authenticatedFlag) {
            if (log.isDebugEnabled()) {
                log.debug("account: {} authenticated fail", account);
            }
            return ApiResponse.ofError("username or password not incorrect");
        }
        List<String> ownRole = accountService.loadAccountRoles(account.getUsername());
        String jwt = JsonWebTokenUtil.issueJwt(UUID.randomUUID().toString(), account.getUsername(),
                "tom-auth-server", 3600L, ownRole);
        Map<String, String> responseData = Collections.singletonMap("token", jwt);
        if (log.isDebugEnabled()) {
            log.debug("issue token success, account: {} -- token: {}", account, jwt);
        }
        redisTokenStore.save(account.getUsername(),jwt);
        return ApiResponse.ofSuccess(responseData);
    }

    @WithoutAuth(mapping = "/auth/register",method = "accountRegister")
    @ApiOperation(value = "注册账号",notes="注册账号")
    @PostMapping("/auth/register")
    public ApiResponse accountRegister(@RequestBody  @Validated Account account) {
        if (accountService.registerAccount(account)) {
            if (log.isDebugEnabled()) {
                log.debug("account: {}, sign up success", account);
            }
            return ApiResponse.ofSuccess("sign up success, login after");
        } else {
            return ApiResponse.ofError("username already exist");
        }
    }
}
