package com.kk.demo.managementApi.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kk.demo.user.dto.RolePageDTO;
import com.kk.demo.user.dto.RoleResourceDTO;
import com.kk.demo.user.dto.RoleResourcePageDTO;
import com.kk.demo.user.entity.Resource;
import com.kk.demo.user.entity.Role;

/**
 * @author sishengcao
 * @date 2021/12/17 16:36
 **/
public interface IRoleManageService {

    IPage<Resource> getPageResourceOwnRole(RoleResourcePageDTO dto);

    IPage<Resource> getPageResourceNotOwnRole(RoleResourcePageDTO dto);

    void authorityRoleResource(RoleResourceDTO dto);

    void deleteAuthorityRoleResource(RoleResourceDTO dto);

    boolean addRole(Role role);

    boolean updateRole(Role role);

    boolean deleteRole(Long roleId);

    IPage<Role> getPageRole(RolePageDTO dto);
}
