package com.kk.demo.managementApi;



/**
 * @author: ziHeng
 * @description: 管理后台网页端接口
 * @create: 2020/04/21 9:10
 */
import com.kk.demo.core.config.ApiConfigSetting;
import com.kk.demo.core.enums.ApiTokenTypeEnums;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 *                    _ooOoo_
 *                   o8888888o
 *                   88" . "88
 *                   (| -_- |)
 *                    O\ = /O
 *                ____/`---'\____
 *              .   ' \\| |// `.
 *               / \\||| : |||// \
 *             / _||||| -:- |||||- \
 *               | | \\\ - /// | |
 *             | \_| ''\---/'' | |
 *              \ .-\__ `-` ___/-. /
 *           ___`. .' /--.--\ `. . __
 *        ."" '< `.___\_<|>_/___.' >'"".
 *       | | : `- \`.;`\ _ /`;.`/ - ` : | |
 *         \ \ `-. \_ __\ /__ _/ .-` / /
 * ======`-.____`-.___\_____/___.-`____.-'======
 *                    `=---='
 *
 * .............................................
 *          佛祖保佑             永无BUG
 */
@SpringBootApplication(scanBasePackages = {"com.kk.demo"})
@MapperScan(basePackages = {"com.kk.demo.*.mapper"})
@EnableAsync(proxyTargetClass = true)
@EnableCaching
@EnableSwagger2
@ServletComponentScan(basePackages = {"com.kk.demo"})
public class ManagementApiApplication extends SpringBootServletInitializer implements ApiConfigSetting {

    @Override
    public ApiTokenTypeEnums getApiTypeEnums() {
        return ApiTokenTypeEnums.SYSTEM;
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(ManagementApiApplication.class);
    }

    @PostConstruct
    void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
    }

    public static void main(String[] args) {
        SpringApplication.run(ManagementApiApplication.class, args);
    }


}
