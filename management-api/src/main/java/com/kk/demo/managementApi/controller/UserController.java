package com.kk.demo.managementApi.controller;

import com.kk.demo.core.response.ApiResponse;
import com.kk.demo.managementApi.service.IUserRoleManageService;
import com.kk.demo.sureness.annotations.PUser;
import com.kk.demo.user.dto.UserRoleDTO;
import com.kk.demo.user.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 *
 * @author tomsun28
 * @date 21:05 2018/3/17
 */
@RestController
@Api(tags = "用户管理")
public class UserController {
    
    @Autowired
    private IUserRoleManageService manageService;


    @PostMapping("/user/role")
    @ApiOperation(value = "获取当前用户的角色",notes = "获取当前用户的角色")
    public ApiResponse getUserRoles(@PUser User user) {
        List<String> roles = manageService.loadAccountRoles(user.getUsername());
        return ApiResponse.ofSuccess(roles);
    }

    @ApiOperation(value = "添加用户权限",notes = "添加用户权限")
    @PostMapping("/user/authority/role")
    public ApiResponse authorityUserRole(@PUser User user,@RequestBody  UserRoleDTO dto) {
        String username = user.getUsername();
        if (!username.equals(dto.getUsername())) {
            return ApiResponse.ofError(ApiResponse.Status.FORBIDDEN);
        }

        boolean flag = manageService.authorityUserRole(dto);
        return flag ? ApiResponse.ofSuccess() : ApiResponse.ofError(ApiResponse.Status.CONFLICT);
    }

    @ApiOperation(value = "删除用户权限",notes = "删除用户权限")
    @PostMapping("/user/authority/role/delete")
    public ApiResponse deleteAuthorityUserRole(@PUser User user,@RequestBody  UserRoleDTO dto) {
        String username = user.getUsername();
        if (!username.equals(dto.getUsername())) {
            return ApiResponse.ofError(ApiResponse.Status.FORBIDDEN);
        }
        return manageService.deleteAuthorityUserRole(dto) ?
            ApiResponse.ofSuccess() : ApiResponse.ofError(ApiResponse.Status.CONFLICT);
    }
}
