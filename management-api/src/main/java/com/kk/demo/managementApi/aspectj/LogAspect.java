//package com.kk.demo.managementApi.aspectj;
//
//import com.alibaba.fastjson.JSON;
//import com.kk.demo.common.annotation.SysLog;
//import com.kk.demo.common.entity.CommonSystemOperateLog;
//import com.kk.demo.common.service.SystemOperateLogService;
//import com.kk.demo.core.response.ApiResponse;
//import com.kk.demo.core.utils.ServletUtils;
//import com.kk.demo.core.utils.ip.AddressUtils;
//import com.kk.demo.core.utils.ip.IpUtils;
//import com.kk.demo.core.utils.logback.LogUtils;
//import com.kk.demo.sureness.config.CurrentUserUtil;
//import com.kk.demo.user.entity.User;
//import com.kk.demo.user.service.IUserService;
//import eu.bitwalker.useragentutils.UserAgent;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.Signature;
//import org.aspectj.lang.annotation.AfterReturning;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Pointcut;
//import org.aspectj.lang.reflect.MethodSignature;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.lang.reflect.Method;
//import java.util.Optional;
//
///**
// * 操作日志记录处理
// */
//@Aspect
//@Component
//public class LogAspect {
//
//    @Autowired
//    private IUserService userService;
//
//    @Autowired
//    private SystemOperateLogService commonSystemOperateLogService;
//
//    // 配置织入点
//    @Pointcut("@annotation(SysLog)")
//    public void logPointCut() {
//    }
//
//    /**
//     * 处理完请求后执行
//     *
//     * @param joinPoint 切点
//     */
//    @AfterReturning(pointcut = "logPointCut()", returning = "jsonResult")
//    public void doAfterReturning(JoinPoint joinPoint, Object jsonResult) {
//        handleLog(joinPoint, jsonResult);
//    }
//
//    protected void handleLog(final JoinPoint joinPoint, Object jsonResult) {
//        try {
//            // 获得注解
//            SysLog controllerLog = getAnnotationLog(joinPoint);
//            if (controllerLog == null) {
//                return;
//            }
//
//            // 数据库日志
//            CommonSystemOperateLog operateLog = new CommonSystemOperateLog();
//            final UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
//            String ip = IpUtils.getIpAddr(ServletUtils.getRequest());
//            if (ip.contains(",")) {
//                String[] ipSplit = ip.split(",");
//                if (ipSplit.length > 0) {
//                    ip = ipSplit[0];
//                }
//            }
//            String address = AddressUtils.getRealAddressByIP(ip);
//            operateLog.setOperIp(ip);
//            operateLog.setOperLocation(address);
//            operateLog.setOperBrowser(userAgent.getBrowser().getName());
//
//            // 获取当前的用户
//            String username = CurrentUserUtil.getUsername();
//            Optional<User> optional = userService.findUserByUsername(username);
//            if(optional.isPresent()){
//                operateLog.setOperSysName(optional.get().getUsername());
//                operateLog.setOperSysUid(optional.get().getId());
//            }
//
//            // 设置方法名称
//            String className = joinPoint.getTarget().getClass().getName();
//            String methodName = joinPoint.getSignature().getName();
//            operateLog.setMethod(className + "." + methodName + "()");
//            // 处理设置注解上的参数
//            getControllerMethodDescription(joinPoint, controllerLog, operateLog,jsonResult);
//            // 保存数据库
//            commonSystemOperateLogService.save(operateLog);
//        } catch (Throwable exp) {
//            // 记录本地异常日志
//            LogUtils.systemLog.error("==前置通知异常==");
//            LogUtils.systemLog.error("异常信息:{}", exp.getMessage());
//            exp.printStackTrace();
//        }
//    }
//
//    /**
//     * 获取注解中对方法的描述信息 用于Controller层注解
//     *
//     * @param log        日志
//     * @param operateLog 操作日志
//     */
//    public void getControllerMethodDescription(JoinPoint joinPoint, SysLog log, CommonSystemOperateLog operateLog,
//                                               Object jsonResult) throws Throwable {
//        // 设置action动作
//        operateLog.setOperType(log.operType().ordinal());
//        // 设置标题
//        operateLog.setTitle(log.title().getModuleName());
//        // 设置操作人类别
//        operateLog.setUserType(log.userType().ordinal());
//        // 是否需要保存request，参数和值
//        if (log.isSaveRequestData()) {
//            String str = JSON.toJSONString(joinPoint.getArgs());
//            operateLog.setRequestJsonStr(str);
//        }
//        if (log.isSaveResponseData()) {
//            if(jsonResult instanceof ApiResponse){
//                ApiResponse response = (ApiResponse)jsonResult;
//                operateLog.setResponseJsonStr(response.getData().toString());
//            }
//        }
//    }
//
//    /**
//     * 是否存在注解，如果存在就获取
//     */
//    private SysLog getAnnotationLog(JoinPoint joinPoint) {
//        Signature signature = joinPoint.getSignature();
//        MethodSignature methodSignature = (MethodSignature) signature;
//        Method method = methodSignature.getMethod();
//
//        if (method != null) {
//            return method.getAnnotation(SysLog.class);
//        }
//        return null;
//    }
//
//    /**
//     * 参数拼装
//     */
//    private String argsArrayToString(Object[] paramsArray) {
//        StringBuilder params = new StringBuilder();
//        if (paramsArray != null && paramsArray.length > 0) {
//            for (Object o : paramsArray) {
//                if (!isFilterObject(o)) {
//                    Object jsonObj = JSON.toJSON(o);
//                    params.append(jsonObj.toString()).append(" ");
//                }
//            }
//        }
//        return params.toString().trim();
//    }
//
//    /**
//     * 判断是否需要过滤的对象。
//     *
//     * @param o 对象信息。
//     * @return 如果是需要过滤的对象，则返回true；否则返回false。
//     */
//    public boolean isFilterObject(final Object o) {
//        return o instanceof MultipartFile || o instanceof HttpServletRequest || o instanceof HttpServletResponse;
//    }
//}
