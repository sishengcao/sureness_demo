package com.kk.demo.managementApi.controller;

import com.kk.demo.core.response.ApiResponse;
import com.kk.demo.managementApi.service.IRoleManageService;
import com.kk.demo.user.dto.RolePageDTO;
import com.kk.demo.user.dto.RoleResourceDTO;
import com.kk.demo.user.dto.RoleResourcePageDTO;
import com.kk.demo.user.entity.Role;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tomsun28
 * @date 00:24 2019-08-01
 */
@RestController
@Slf4j
@Api(tags = "角色管理")
public class RoleController {

    @Autowired
    private IRoleManageService manageService;

    @ApiOperation(value = "根据角色ID获取资源分页",notes = "根据角色ID获取资源分页")
    @PostMapping("/role/resource/page")
    public ApiResponse getResourceOwnByRole(@RequestBody  RoleResourcePageDTO dto) {
        return ApiResponse.ofSuccess(manageService.getPageResourceOwnRole(dto));
    }

    @ApiOperation(value = "根据角色ID获取公共资源分页",notes = "根据角色ID获取公共资源分页")
    @PostMapping("/role/resource/notOwn/page")
    public ApiResponse getResourceNotOwnByRole(@RequestBody  RoleResourcePageDTO dto) {
        return ApiResponse.ofSuccess(manageService.getPageResourceNotOwnRole(dto));
    }

    @ApiOperation(value = "根据角色ID和资源ID获取资源列表",notes = "根据角色ID和资源ID获取资源列表")
    @PostMapping("/role/authority/resource")
    public ApiResponse authorityRoleResource(@RequestBody  RoleResourceDTO dto) {
        manageService.authorityRoleResource(dto);
        return ApiResponse.ofSuccess();
    }

    @ApiOperation(value = "根据角色ID删除资源",notes = "根据角色ID删除资源")
    @PostMapping("/role/authority/resource/delete")
    public ApiResponse deleteAuthorityRoleResource(@RequestBody  RoleResourceDTO dto) {
        manageService.deleteAuthorityRoleResource(dto);
        return ApiResponse.ofSuccess();
    }

    @ApiOperation(value = "根据角色添加资源",notes = "根据角色添加资源")
    @PostMapping("/role/add")
    public ApiResponse addRole(@RequestBody @Validated  Role role) {
        if (manageService.addRole(role)) {
            if (log.isDebugEnabled()) {
                log.debug("add role success: {}", role);
            }
            return ApiResponse.ofSuccess();
        } else {
            return ApiResponse.ofError("role already exist");
        }
    }

    @ApiOperation(value = "根据角色更新资源",notes = "根据角色更新资源")
    @PostMapping("/role/update")
    public ApiResponse updateRole(@RequestBody @Validated  Role role) {
        if (manageService.updateRole(role)) {
            if (log.isDebugEnabled()) {
                log.debug("update role success: {}", role);
            }
            return ApiResponse.ofSuccess();
        } else {
            return ApiResponse.ofError("role not exist");
        }
    }

    @ApiOperation(value = "根据角色删除资源",notes = "根据角色删除资源")
    @PostMapping("/role/delete")
    public ApiResponse deleteRole(@RequestBody  Role role) {
        if (manageService.deleteRole(role.getId())) {
            if (log.isDebugEnabled()) {
                log.debug("delete role success: {}", role.getId());
            }
            return ApiResponse.ofSuccess();
        } else {
            log.debug("delete role fail: {}", role.getId());
            return ApiResponse.ofError("delete role fail, no this role here");
        }
    }

    @ApiOperation(value = "角色列表(分页)",notes = "角色列表(分页)")
    @PostMapping("/role/page")
    public ApiResponse getRole(@RequestBody  RolePageDTO dto) {
        return ApiResponse.ofSuccess(manageService.getPageRole(dto));
    }

}
