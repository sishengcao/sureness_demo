package com.kk.demo.managementApi.service.impl;

import com.kk.demo.managementApi.service.AccountService;
import com.kk.demo.user.dto.Account;
import com.kk.demo.user.entity.User;
import com.kk.demo.user.entity.UserRole;
import com.kk.demo.user.service.IUserRoleService;
import com.kk.demo.user.service.IUserService;
import com.usthe.sureness.util.Md5Util;
import com.usthe.sureness.util.SurenessCommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author tomsun28
 * @date 10:58 2019-08-04
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AccountServiceImpl implements AccountService {

    @Autowired
    private IUserService userService;

    private IUserRoleService userRoleService;

    @Override
    public boolean authenticateAccount(Account account) {
        Optional<User> authUserOptional = userService.findUserByUsername(account.getUsername());
        if (!authUserOptional.isPresent()) {
            return false;
        }
        User authUser = authUserOptional.get();
        String password = account.getPassword();
        if (password == null) {
            return false;
        }
        if (Objects.nonNull(authUser.getSalt())) {
            // md5 with salt
            password = Md5Util.md5(password + authUser.getSalt());

        }
        return authUser.getPassword().equals(password);
    }

    @Override
    public List<String> loadAccountRoles(String username) {
        return userService.findAccountOwnRoles(username);
    }

    @Override
    public boolean registerAccount(Account account) {
        if (isAccountExist(account)) {
            return false;
        }
        String salt = SurenessCommonUtil.getRandomString(6);
        String password = Md5Util.md5(account.getPassword() + salt);
        User authUser = User.builder().username(account.getUsername())
                .password(password).salt(salt).status(1).build();
        userService.save(authUser);
        return true;
    }

    @Override
    public boolean isAccountExist(Account account) {
        Optional<User> authUserOptional = userService.findUserByUsername(account.getUsername());
        return authUserOptional.isPresent();
    }

    @Override
    public boolean authorityUserRole(String appId, Long roleId) {
        Optional<User> optional = userService.findUserByUsername(appId);
        if (!optional.isPresent()) {
            return false;
        }
        Long userId = optional.get().getId();
        UserRole userRoleBindDO = UserRole.builder().userId(userId).roleId(roleId).build();

        userRoleService.save(userRoleBindDO);
        return true;
    }

    @Override
    public boolean deleteAuthorityUserRole(String appId, Long roleId) {
        Optional<User> optional = userService.findUserByUsername(appId);
        if (!optional.isPresent()) {
            return false;
        }
        Long userId = optional.get().getId();
        userRoleService.deleteRoleResourceBind(roleId, userId);
        return true;
    }

}
