package com.kk.demo.managementApi.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kk.demo.user.dto.ResourcePageDTO;
import com.kk.demo.user.entity.Resource;

/**
 * @author sishengcao
 * @date 2021/12/17 16:35
 **/
public interface IResourceManageService {
    IPage<Resource> getPage(ResourcePageDTO dto);

    boolean addResource(Resource authResource);

    boolean updateResource(Resource authResource);

    boolean deleteResource(Long id);
}
