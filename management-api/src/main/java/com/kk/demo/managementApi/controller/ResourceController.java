package com.kk.demo.managementApi.controller;

import com.kk.demo.core.response.ApiResponse;
import com.kk.demo.managementApi.service.IResourceManageService;
import com.kk.demo.user.dto.ResourcePageDTO;
import com.kk.demo.user.entity.Resource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tomsun28
 * @date 00:24 2019-08-01
 */
@RestController
@Slf4j
@Api(tags = "资源权限")
public class ResourceController {

    @Autowired
    private IResourceManageService manageService;

    @PostMapping("/resource/page")
    @ApiOperation(value = "获取分页",notes = "获取分页")
    public ApiResponse getResource(@RequestBody  ResourcePageDTO dto) {
        return ApiResponse.ofSuccess(manageService.getPage(dto));
    }

    @ApiOperation(value = "添加资源",notes = "添加资源")
    @PostMapping("/resource/add")
    public ApiResponse addResource(@RequestBody  @Validated Resource authResource) {
        if (manageService.addResource(authResource)) {
            if (log.isDebugEnabled()) {
                log.debug("add resource success: {}", authResource);
            }
            return ApiResponse.ofSuccess();
        } else {
            return ApiResponse.ofError("resource already exist");
        }
    }

    @ApiOperation(value = "更新资源",notes = "更新资源")
    @PostMapping("/resource/update")
    public ApiResponse updateResource(@RequestBody  @Validated Resource authResource) {
        if (manageService.updateResource(authResource)) {
            if (log.isDebugEnabled()) {
                log.debug("update resource success: {}", authResource);
            }
            return ApiResponse.ofSuccess();
        } else {
            return ApiResponse.ofError("resource not exist");
        }
    }

    @ApiOperation(value = "删除资源",notes = "删除资源")
    @PostMapping("/resource/delete")
    public ApiResponse deleteResource(@RequestBody  Resource authResource) {
        if (manageService.deleteResource(authResource.getId())) {
            if (log.isDebugEnabled()) {
                log.debug("delete resource success: {}", authResource.getId());
            }
            return ApiResponse.ofSuccess();
        } else {
            log.error("delete resource fail: {}", authResource.getId());
            return ApiResponse.ofError("delete resource fail, please try again later");
        }
    }

}
