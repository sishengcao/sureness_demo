package com.kk.demo.managementApi.service;

import com.kk.demo.user.dto.UserRoleDTO;

import java.util.List;

/**
 * @author sishengcao
 * @date 2021/12/17 16:36
 **/
public interface IUserRoleManageService {
    List<String> loadAccountRoles(String appId);

    boolean authorityUserRole(UserRoleDTO dto);

    boolean deleteAuthorityUserRole(UserRoleDTO dto);
}
