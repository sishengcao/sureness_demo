package com.kk.demo.managementApi.service.impl;/**
 * @author sishengcao
 * @date 2021/12/17 16:35
 **/

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kk.demo.managementApi.service.IResourceManageService;
import com.kk.demo.user.dto.ResourcePageDTO;
import com.kk.demo.user.entity.Resource;
import com.kk.demo.user.service.IResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author sishengcao
 * @date 2021年12月17日 16:35
 * @describe : 
 */
@Service
public class ResourceManageServiceImpl implements IResourceManageService {

    @Autowired
    private IResourceService resourceService;

    @Override public IPage<Resource> getPage(ResourcePageDTO dto) {
        QueryWrapper<Resource> query = new QueryWrapper<>();
        return resourceService.page(new Page<>(dto.getPageNum(),dto.getPageSize()),query);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean addResource(Resource authResource) {
        return resourceService.save(authResource);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean updateResource(Resource authResource) {
        return resourceService.updateById(authResource);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteResource(Long id) {
        return resourceService.removeById(id);
    }
}
