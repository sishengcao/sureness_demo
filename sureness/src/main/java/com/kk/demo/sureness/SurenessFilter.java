package com.kk.demo.sureness;

import com.kk.demo.core.response.ApiResponse;
import com.kk.demo.core.utils.ResponseUtil;
import com.kk.demo.sureness.exception.RefreshExpiredTokenException;
import com.usthe.sureness.mgt.SurenessSecurityManager;
import com.usthe.sureness.processor.exception.*;
import com.usthe.sureness.subject.SubjectSum;
import com.usthe.sureness.util.SurenessContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * sureness filter class example, filter all http request
 * @author tomsun28
 * @date 23:22 2020-03-02
 */
@Order(1)
@WebFilter(filterName = "SurenessFilter", urlPatterns = "/*", asyncSupported = true)
public class SurenessFilter implements Filter {

    /** logger **/
    private static final Logger logger = LoggerFactory.getLogger(SurenessFilter.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        try {
            SubjectSum subject = SurenessSecurityManager.getInstance().checkIn(servletRequest);
            // You can consider using SurenessContextHolder to bind subject in threadLocal
            // if bind, please remove it when end
            if (subject != null) {
                SurenessContextHolder.bindSubject(subject);
            }
        } catch (IncorrectCredentialsException | UnknownAccountException | ExpiredCredentialsException e1) {
            logger.debug("this request account info is illegal, {}", e1.getMessage());
            ResponseUtil.toResponseEncryptWriter((HttpServletResponse)servletResponse, ApiResponse.ofError(ApiResponse.Status.UNAUTHORIZED,
                "Username or password is incorrect or expired"));
        } catch (DisabledAccountException | ExcessiveAttemptsException e2 ) {
            logger.debug("the account is disabled, {}", e2.getMessage());
            ResponseUtil.toResponseEncryptWriter((HttpServletResponse)servletResponse, ApiResponse.ofError(ApiResponse.Status.UNAUTHORIZED,
                "Account is disabled"));
        } catch (RefreshExpiredTokenException e4) {
            logger.debug("this account credential token is expired, return refresh value");
            ResponseUtil.toResponseEncryptWriter((HttpServletResponse)servletResponse, ApiResponse.ofError(ApiResponse.Status.UNAUTHORIZED,
                "refresh-token"));
        } catch (UnauthorizedException e5) {
            logger.debug("this account can not access this resource, {}", e5.getMessage());
            ResponseUtil.toResponseEncryptWriter((HttpServletResponse)servletResponse, ApiResponse.ofError(ApiResponse.Status.FORBIDDEN,
                "This account has no permission to access this resource"));
        } catch (RuntimeException e) {
            logger.error("other exception happen: ", e);
            ResponseUtil.toResponseEncryptWriter((HttpServletResponse)servletResponse, ApiResponse.ofError(ApiResponse.Status.SYSTEM_FAILED));
        }
        try {
            // if ok, doFilter and add subject in request
            filterChain.doFilter(servletRequest, servletResponse);
        } finally {
            SurenessContextHolder.clear();
        }
    }
}
