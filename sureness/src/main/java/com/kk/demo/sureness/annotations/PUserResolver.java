package com.kk.demo.sureness.annotations;

import com.kk.demo.core.config.ApiConfigSetting;
import com.kk.demo.core.enums.ApiTokenTypeEnums;
import com.kk.demo.core.enums.StatusEnum;
import com.kk.demo.core.exceptions.BusinessException;
import com.kk.demo.core.response.ApiResponse;
import com.kk.demo.sureness.config.CurrentUserUtil;
import com.kk.demo.user.entity.User;
import com.kk.demo.user.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.Optional;

@Component
@Slf4j
public class PUserResolver implements HandlerMethodArgumentResolver {

    @Autowired
    private IUserService userService;

    @Autowired
    private ApiConfigSetting apiConfigSetting;

    @Autowired
    private CurrentUserUtil currentUserUtil;

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.hasParameterAnnotation(PUser.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        HttpServletRequest httpServletRequest = nativeWebRequest.getNativeRequest(HttpServletRequest.class);
        if (httpServletRequest == null) {
            throw new BusinessException(ApiResponse.Status.SYSTEM_ERROR_HEADERS);
        }
        String username = currentUserUtil.getUsername();
        if(Objects.isNull(username)){
            throw new BusinessException(ApiResponse.Status.SYSTEM_ERROR_TOKEN);
        }
        Object user = null;
        Integer status = StatusEnum.N.getStatus();
        //app端token userId从user表获取
        if (apiConfigSetting.getApiTypeEnums() == ApiTokenTypeEnums.APP ||
                apiConfigSetting.getApiTypeEnums() == ApiTokenTypeEnums.WEB) {
            //todo 尚无这类用户
        } else if (apiConfigSetting.getApiTypeEnums() == ApiTokenTypeEnums.SYSTEM) {//系统用户从sysUser表获取
            Optional<User> systemUser = userService.findUserByUsername(username);
            user = systemUser.get();
            status = ((User) user).getStatus();
        }
        //用户登录异常
        if (user == null) {
            throw new BusinessException(ApiResponse.Status.USER_LOGIN_EXPIRE);
        }
        //用户冻结
        if (StatusEnum.N.getStatus() == status) {
            throw new BusinessException(ApiResponse.Status.USER_ACCOUNT_FREEZE);
        }
        return user;


    }
}
