package com.kk.demo.sureness.config;/**
 * @author sishengcao
 * @date 2021/12/21 12:16
 **/

import com.kk.demo.core.exceptions.BusinessException;
import com.kk.demo.core.response.ApiResponse;
import com.kk.demo.core.utils.HttpServletUtil;
import com.kk.demo.sureness.tokenStore.RedisTokenStore;
import com.usthe.sureness.subject.SubjectSum;
import com.usthe.sureness.util.SurenessContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @author sishengcao
 * @date 2021年12月21日 12:16
 * @describe : 
 */
@Component
public class CurrentUserUtil {

    @Autowired
    private RedisTokenStore redisTokenStore;

    @Autowired
    private HttpServletUtil httpServletUtil;

    public String getUsername(){
        SubjectSum subject = SurenessContextHolder.getBindSubject();
        if (subject == null || subject.getPrincipal() == null) {
            HttpServletRequest httpServletRequest = httpServletUtil.getRequest();
            String token = httpServletRequest.getHeader("Authorization");
            if(Objects.nonNull(token)){
                if(redisTokenStore.tokenExpired(token,httpServletRequest)){
                    String username = redisTokenStore.getUsernameByToken(token);
                    if(Objects.nonNull(username)){
                        return username;
                    }
                }
            }
            throw new BusinessException(ApiResponse.Status.FORBIDDEN);
        }
        String username = (String) subject.getPrincipal();
        return username;
    }
}
