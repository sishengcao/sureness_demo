package com.kk.demo.sureness.service.impl;

import com.kk.demo.sureness.service.IAccountService;
import com.kk.demo.user.entity.User;
import com.kk.demo.user.service.IUserService;
import com.usthe.sureness.provider.DefaultAccount;
import com.usthe.sureness.provider.SurenessAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author sishengcao
 * @date 2021/12/17 15:11
 **/
@Service("surenessAccountServiceImpl")
public class AccountServiceImpl implements IAccountService {
    
    @Autowired
    private IUserService userService;

    @Override
    public SurenessAccount loadAccount(String appId) {
        Optional<User> authUserOptional = userService.findUserByUsername(appId);
        if (authUserOptional.isPresent()) {
            User authUser = authUserOptional.get();
            DefaultAccount.Builder accountBuilder = DefaultAccount.builder(appId)
                .setPassword(authUser.getPassword())
                .setSalt(authUser.getSalt())
                .setDisabledAccount(1 != authUser.getStatus())
                .setExcessiveAttempts(2 == authUser.getStatus());
            List<String> roles = userService.findAccountOwnRoles(appId);
            if (roles != null) {
                accountBuilder.setOwnRoles(roles);
            }
            return accountBuilder.build();
        } else {
            return null;
        }
    }
}
