package com.kk.demo.sureness.service;

import com.usthe.sureness.provider.SurenessAccount;

/**
 * @author sishengcao
 * @date 2021/12/17 15:10
 **/
public interface IAccountService {
    SurenessAccount loadAccount(String appId);
}
