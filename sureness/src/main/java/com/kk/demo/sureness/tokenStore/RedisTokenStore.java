package com.kk.demo.sureness.tokenStore;

import com.kk.demo.core.config.ApiConfigSetting;
import com.kk.demo.core.enums.ApiTokenTypeEnums;
import com.kk.demo.core.utils.HttpServletUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author: ziHeng
 * @description:
 * @create: 2020/05/23 09:44
 */
@Component
public class RedisTokenStore {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ApiConfigSetting apiConfigSetting;

    @Autowired
    private HttpServletUtil httpServletUtil;


    private String userIdByTokenKey(String token) {
        ApiTokenTypeEnums apiTokenTypeEnums = apiConfigSetting.getApiTypeEnums();
        return apiTokenTypeEnums.getType() + ":userIdByTokenKey-" + httpServletUtil.getVisitMethod() +
                "-" + apiConfigSetting.getApiTypeEnums().getValue() + "-" + token;
    }

    public String userIdByTokenKey(String token,HttpServletRequest request) {
        ApiTokenTypeEnums apiTokenTypeEnums = apiConfigSetting.getApiTypeEnums();
        return apiTokenTypeEnums.getType() + ":userIdByTokenKey-" + httpServletUtil.getVisitMethod(request) +
                "-" + apiConfigSetting.getApiTypeEnums().getValue() + "-" + token;
    }


    private String tokenByUserIdKey(String userId) {
        ApiTokenTypeEnums apiTokenTypeEnums = apiConfigSetting.getApiTypeEnums();
        return apiTokenTypeEnums.getType() + ":tokenByUserIdKey-" + httpServletUtil.getVisitMethod() +
                "-" + apiConfigSetting.getApiTypeEnums().getValue() + "-" + userId;
    }

    //存放token
    public void save(String userId, String token) {
        long second = apiConfigSetting.getApiTypeEnums().getTokenExpirationTime();
        //一个token一个设备登录  若注释掉 一个token可以多设备登录
        releaseByUserId(userId);
        stringRedisTemplate.opsForValue().set(userIdByTokenKey(token), userId, second, TimeUnit.SECONDS);
        stringRedisTemplate.opsForValue().set(tokenByUserIdKey(userId), token, second, TimeUnit.SECONDS);
    }

    //通过用户id删除其状态
    public void releaseByUserId(String userId) {
        Object myToken = stringRedisTemplate.opsForValue().get(tokenByUserIdKey(userId));
        if (myToken != null) {
            String myTokenString = myToken.toString();
            stringRedisTemplate.delete(userIdByTokenKey(myTokenString));
            stringRedisTemplate.delete(tokenByUserIdKey(userId));
        }
    }

    //token删除其状态
    public void releaseByToken(String token) {
        Object userIdByTokenKey = stringRedisTemplate.opsForValue().get(userIdByTokenKey(token));
        if (userIdByTokenKey != null) {
            String userId = userIdByTokenKey.toString();
            stringRedisTemplate.delete(userIdByTokenKey(token));
            stringRedisTemplate.delete(tokenByUserIdKey(userId));
        }
    }

    //判断用户状态是否过期
    public boolean tokenExpired(String token, HttpServletRequest request) {
        Boolean hasKey = stringRedisTemplate.hasKey(userIdByTokenKey(token,request));
        return hasKey != null && !hasKey;
    }


    public long getExpirationTime() {
        return apiConfigSetting.getApiTypeEnums().getTokenExpirationTime();
    }

    public String getUsernameByToken(String token){
        Object userIdByTokenKey = stringRedisTemplate.opsForValue().get(userIdByTokenKey(token));
        if(Objects.nonNull(userIdByTokenKey)){
            return (String)userIdByTokenKey;
        }
        return null;
    }


}
