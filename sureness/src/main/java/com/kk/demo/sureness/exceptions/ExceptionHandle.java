package com.kk.demo.sureness.exceptions;

import com.kk.demo.core.exceptions.BusinessException;
import com.kk.demo.core.exceptions.IllegalParameterException;
import com.kk.demo.core.response.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * class_name:ExceptionHandle
 *
 * @Author:LiuNing
 * @Description: 统一异常处理类 （所有异常统一处理）
 * @Date:15:19 2017/11/9
 * @Version:1.0
 */

@ControllerAdvice
public class ExceptionHandle {

    private final static Logger logger = LoggerFactory.getLogger(ExceptionHandle.class);

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ApiResponse handle(Exception e) {
        if (e instanceof DuplicateKeyException || e instanceof SQLIntegrityConstraintViolationException) {
            return ApiResponse.ofError(ApiResponse.Status.SYSTEM_DUPLICATE_RECORD);
        } else if (e instanceof BusinessException) {
            return ApiResponse.ofError((BusinessException) e);
        }
        if (e instanceof IllegalParameterException) {
            return ApiResponse.ofError(e.getMessage());
        } else {
            logger.error("系统异常", e);
            return ApiResponse.ofError(ApiResponse.Status.BUSINESS_ERROR);
        }
    }
}
