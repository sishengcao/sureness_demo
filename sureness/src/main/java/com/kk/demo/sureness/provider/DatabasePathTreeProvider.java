package com.kk.demo.sureness.provider;

import com.kk.demo.core.config.PermitUrlConfig;
import com.kk.demo.user.service.IResourceService;
import com.usthe.sureness.matcher.PathTreeProvider;
import com.usthe.sureness.util.SurenessCommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Set;

/**
 * ths provider provides path resources
 * load sureness config resource form database
 * @author tomsun28
 * @date 16:00 2019-08-04
 */
@Component
public class DatabasePathTreeProvider implements PathTreeProvider {

    @Autowired
    private IResourceService resourceService;

    @Autowired
    private PermitUrlConfig permitUrlConfig;

    @Override
    public Set<String> providePathData() {
        Set<String> set = resourceService.getAllEnableResourcePath();
        return SurenessCommonUtil.attachContextPath(getContextPath(), set);

    }

    @Override
    public Set<String> provideExcludedResource() {
        Set<String> exclude = resourceService.getAllOpenResourcePath();
        exclude.addAll(Arrays.asList(permitUrlConfig.getUrls()));
        return SurenessCommonUtil.attachContextPath(getContextPath(), exclude);
    }

}
