package com.kk.demo.sureness.subject;/**
 * @author sishengcao
 * @date 2021/12/20 10:20
 **/

import com.kk.demo.sureness.tokenStore.RedisTokenStore;
import com.kk.demo.user.service.IUserService;
import com.usthe.sureness.subject.PrincipalMap;
import com.usthe.sureness.subject.Subject;
import com.usthe.sureness.subject.creater.SessionSubjectServletCreator;
import com.usthe.sureness.subject.support.SessionSubject;
import com.usthe.sureness.util.SurenessConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;

/**
 * @author sishengcao
 * @date 2021年12月20日 10:20
 * @describe : 
 */
@Configuration
public class RedisSessionSubject extends SessionSubjectServletCreator {

    private Logger logger = LoggerFactory.getLogger(RedisSessionSubject.class);

    private static final String HEADER_TOKEN = SurenessConstant.AUTHORIZATION;

    @Autowired
    private RedisTokenStore redisTokenStore;

    @Autowired
    private IUserService userService;

    @Override
    public boolean canSupportSubject(Object context) {
        if (!(context instanceof HttpServletRequest)) {
            return false;
        } else {
            HttpSession httpSession = ((HttpServletRequest)context).getSession(false);
            HttpServletRequest servletRequest = (HttpServletRequest)context;
            String token = servletRequest.getHeader(HEADER_TOKEN);
            if(Objects.isNull(token)){
                String username = checkToken(token,servletRequest);
                if(Objects.isNull(username)){
                    return false;
                }
            }
            return httpSession != null && httpSession.getAttribute("principal") != null;
        }
    }

    @Override
    public Subject createSubject(Object context) {
        HttpServletRequest servletRequest = (HttpServletRequest)context;
        HttpSession httpSession = servletRequest.getSession(false);
        String principal = (String)httpSession.getAttribute("principal");
        if (principal != null && !"".equals(principal.trim())) {
            Object principalMapTmp = httpSession.getAttribute("principals");
            PrincipalMap principalMap = principalMapTmp == null ? null : (PrincipalMap)principalMapTmp;
            Object rolesTmp = httpSession.getAttribute("roles");
            List<String> roles = rolesTmp == null ? null : (List)rolesTmp;
            return buildSubject(servletRequest,roles,principal,principalMap);
        } else {
            String token = servletRequest.getHeader(HEADER_TOKEN);
            String username = checkToken(token,servletRequest);
            if(Objects.isNull(username)){
                return null;
            }
            List<String> ownRole = userService.findAccountOwnRoles(username);
            return buildSubject(servletRequest,ownRole,username,null);
        }
    }

    private String checkToken(String token,HttpServletRequest servletRequest){
        if(Objects.isNull(token)) {
            return null;
        }
        if (redisTokenStore.tokenExpired(token, servletRequest)) {
            logger.error("token过期");
            return null;
        }
        String username = redisTokenStore.userIdByTokenKey(token,servletRequest);
        if(Objects.isNull(username)){
            return null;
        }
        return username;
    }

    private Subject buildSubject(HttpServletRequest servletRequest,List<String> ownRole,String principal,PrincipalMap principalMap){
        String remoteHost = servletRequest.getRemoteHost();
        String requestUri = servletRequest.getRequestURI();
        String requestType = servletRequest.getMethod();
        String targetUri = requestUri.concat("===").concat(requestType).toLowerCase();
        return SessionSubject.builder(principal, ownRole).setPrincipalMap(principalMap).setRemoteHost(remoteHost).setTargetResource(targetUri).build();
    }
}
