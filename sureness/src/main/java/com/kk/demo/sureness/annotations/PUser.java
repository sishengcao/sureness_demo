package com.kk.demo.sureness.annotations;

import java.lang.annotation.*;

//作用在参数
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PUser {

}
