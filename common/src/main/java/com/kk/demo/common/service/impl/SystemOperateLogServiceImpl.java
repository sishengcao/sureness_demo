package com.kk.demo.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kk.demo.common.entity.CommonSystemOperateLog;
import com.kk.demo.common.mapper.SystemOperateLogMapper;
import com.kk.demo.common.service.SystemOperateLogService;
import org.springframework.stereotype.Service;

/**
 * @ClassName CommonSystemOperateLogServiceImpl
 * @Description 平台操作日志实现类
 * @Author July
 * @Date: 2021-08-07 10:41:28
 **/
@Service
public class SystemOperateLogServiceImpl extends ServiceImpl<SystemOperateLogMapper, CommonSystemOperateLog>
        implements SystemOperateLogService {
}
