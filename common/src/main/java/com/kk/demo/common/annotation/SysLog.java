package com.kk.demo.common.annotation;
import com.kk.demo.core.enums.SysOpenModuleEnum;
import com.kk.demo.core.enums.SysOperateTypeEnum;
import com.kk.demo.core.enums.SysOperateUserTypeEnum;

import java.lang.annotation.*;

/**
 * 系统操作日志注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

    /**
     * 模块
     */
    SysOpenModuleEnum title() default SysOpenModuleEnum.OTHER;

    /**
     * 功能
     */
    SysOperateTypeEnum operType() default SysOperateTypeEnum.OTHER;

    /**
     * 操作人类别
     */
    SysOperateUserTypeEnum userType() default SysOperateUserTypeEnum.SYSTEM_USER;

    /**
     * 是否保存请求的参数
     */
     boolean isSaveRequestData() default true;

     /**
      * 是否保存返回参数
      */
     boolean isSaveResponseData() default true;

}
