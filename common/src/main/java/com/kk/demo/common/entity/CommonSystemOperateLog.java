package com.kk.demo.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 后台系统操作日志
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("system_operate_log")
public class CommonSystemOperateLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 操作标题
     */
    @TableField("title")
    private String title;

    /**
     * 业务类型（0=其它,1=新增,2=修改,3=删除,4=授权,5=导出,6=导入）
     */
    @TableField("oper_type")
    private Integer operType;

    /**
     * 方法名称
     */
    @TableField("method")
    private String method;

    /**
     * 用户类型（0：其它 1：pc端用户 2：移动端用户）
     */
    @TableField("user_type")
    private Integer userType;

    /**
     * 操作用户uid
     */
    @TableField("oper_sys_uid")
    private Long operSysUid;

    /**
     * 操作人员
     */
    @TableField("oper_sys_name")
    private String operSysName;

    /**
     * 请求参数
     */
    @TableField("response_json_str")
    private String responseJsonStr;

    /**
     * 返回值
     */
    @TableField("request_json_str")
    private String requestJsonStr;

    /**
     * ip地址
     */
    @TableField("oper_ip")
    private String operIp;

    /**
     * 地址
     */
    @TableField("oper_location")
    private String operLocation;

    /**
     * 操作浏览器
     */
    @TableField("oper_browser")
    private String operBrowser;

    /**
     * 状态（0：无效 1：有效）
     */
    @TableField("state")
    private Integer state;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

}
