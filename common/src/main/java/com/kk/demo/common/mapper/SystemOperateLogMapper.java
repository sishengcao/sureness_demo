package com.kk.demo.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kk.demo.common.entity.CommonSystemOperateLog;
import org.mapstruct.Mapper;

/**
 * 后台系统操作日志 Mapper 接口
 */
@Mapper
public interface SystemOperateLogMapper extends BaseMapper<CommonSystemOperateLog> {

}
