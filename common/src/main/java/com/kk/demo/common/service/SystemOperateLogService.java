package com.kk.demo.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kk.demo.common.entity.CommonSystemOperateLog;

/**
 * @ClassName SystemOperateLogService
 * @Description 平台操作日志
 * @Author July
 * @Date: 2021-08-07 10:40:59
 **/
public interface SystemOperateLogService extends IService<CommonSystemOperateLog> {
}
